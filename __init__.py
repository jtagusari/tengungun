"""TENGUNGUN plugin init"""

__copyright__ = '(C) 2023 by Junta Tagusari'
__license__ = 'GPL version 3'
__email__ = 'j.tagusari@eng.hokudai.ac.jp'

def classFactory(iface):
    from .tengungun import tengungun_plugin        
    return tengungun_plugin(iface)