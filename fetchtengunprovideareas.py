from qgis.PyQt.QtCore import (QT_TRANSLATE_NOOP, QVariant,QSettings,QCoreApplication)
from qgis.core import (
  QgsProcessingParameterExtent,
  QgsProcessing,
  QgsProcessingParameterCrs,
  QgsWkbTypes,
  QgsProcessingParameterFeatureSink,
  QgsField,
  QgsFeature,
  QgsGeometry,
  QgsProcessingParameterEnum,
  QgsProcessingParameterFeatureSource,
  QgsCoordinateReferenceSystem,
  QgsReferencedRectangle,
  QgsProcessingAlgorithm,
  QgsCoordinateTransform,
  QgsProject
  )

from qgis import processing
import copy


from .util.zukakutile import ZukakuTile
from .util.hriskutil import HrUtil
from .util.hriskfields import HrFields
from .util.tengunvar import TengunDatabase

class fetchtengunprovideareas(QgsProcessingAlgorithm):
  
  # UIs
  PARAMETERS = {  
    "FETCH_EXTENT": {
      "ui_func": QgsProcessingParameterExtent,
      "ui_args":{
        "description": QT_TRANSLATE_NOOP("fetchtengunprovideareas","Extent for fetching data")
      }
    },
    "FETCH_POLYGON": {
      "ui_func": QgsProcessingParameterFeatureSource,
      "ui_args":{
        "optional": True,
        "description": QT_TRANSLATE_NOOP("fetchtengunprovideareas","Area for fetching data"),
        "types": [QgsProcessing.TypeVectorPolygon]
      }
    },
    "TARGET_CRS": {
      "ui_func": QgsProcessingParameterCrs,
      "ui_args": {
        "optional": True,
        "description": QT_TRANSLATE_NOOP("fetchtengunprovideareas","CRS for the output of the data list")
      }
    },
    "PC_DATABASE": {
      "ui_func": QgsProcessingParameterEnum,
      "ui_args": {
        "options": [],
        "description": QT_TRANSLATE_NOOP("fetchtengun","Point cloud database"),
        "allowMultiple": True,
        "defaultValue": 0
      }
    },
    "OUTPUT": {
      "ui_func": QgsProcessingParameterFeatureSink,
      "ui_args": {
        "description": QT_TRANSLATE_NOOP("fetchtengunprovideareas","Point-cloud grid")
      }
    }
  }
  
  FIELDS = {}
  
  def __init__(self):
    super().__init__()
    self.UTIL = HrUtil(self)    
    
            
    self.FIELDS["TENGUN_PROVIDED_AREAS"] = HrFields.fromQgsFieldList([
      QgsField("crs", QVariant.String, "string"),
      QgsField("meshcode", QVariant.String, "string"),
      QgsField("description", QVariant.String, "string"),
      QgsField("url", QVariant.String, "string"),
      QgsField("filesize", QVariant.String, "string")
    ])
  
  # initialization of the algorithm
  def initAlgorithm(self, config):        
    (extent, target_crs) = self.UTIL.getExtentAndCrsUsingCanvas()
    self.UTIL.setDefaultValue("FETCH_EXTENT", extent)
    self.UTIL.setDefaultValue("TARGET_CRS", target_crs.authid())
    self.UTIL.initParameters()
    self.PARAMETERS["PC_DATABASE"]["ui_args"]["options"] = [item["description"] for item in TengunDatabase]
    self.PARAMETERS["PC_DATABASE"]["ui_args"]["defaultValue"] = list(range(len(TengunDatabase)))
  
  
  # execution of the algorithm
  def processAlgorithm(self, parameters, context, feedback):    
    # import ptvsd
    # ptvsd.debug_this_thread()
    
    self.UTIL.registerProcessingParameters(parameters, context, feedback)
    # check inputs
    dbs = [TengunDatabase[idx] for idx in self.parameterAsEnums(parameters, "PC_DATABASE", context)]
    crs_output = self.parameterAsCrs(parameters, "TARGET_CRS", context)
    
    if not crs_output.isValid():
      raise Exception(self.tr("Invalid CRS"))
  
    # first, set the fetch area
    # use given extent if the fetch polygon is NOT given. 
    # use the bounding box of the fetch polygon if it is given
    if self.parameterAsSource(parameters, "FETCH_POLYGON", context) is None:
      fetch_area = QgsReferencedRectangle(
        self.parameterAsExtent(parameters, "FETCH_EXTENT", context, QgsCoordinateReferenceSystem(crs_output)),
        crs_output
      )
      
    else:
      # obtain the bbox of the fetch polygon
      fetch_polygon_bbox = processing.run(
        "qgis:minimumboundinggeometry", 
        {
          "INPUT": self.parameterAsSource(parameters, "FETCH_POLYGON", context),
          "TYPE": 0,
          "OUTPUT": "TEMPORARY_OUTPUT"
        },
        context = context,
        feedback = feedback,
        is_child_algorithm=True
      )["OUTPUT"]
      
      fetch_polygon_bbox = context.getMapLayer(fetch_polygon_bbox)
      fetch_area = QgsReferencedRectangle(
        fetch_polygon_bbox.getFeature(1).geometry().boundingBox(),
        fetch_polygon_bbox.crs()
      )
        
  
    # then, specify the mesh grid for each database withing the fetch area    
    meshes = []
    for db in dbs:
      
      # obtain the database information
      pcode_format = db["pcode_format"]
      crs = db['crs']
      base_url = db["url"]
      if "{pcode}" not in base_url:
        raise Exception(self.tr("URL must contain {pcode}") + f"(id: {db['id']})")
      
      tile = ZukakuTile(crs = crs, level =pcode_format["level"])
      
      # for tile in tiles():
      for code in tile.cellMeshCode(fetch_area):
        code = code.lower() if pcode_format.get("case") == "lower" else code
        url = base_url.replace("{pcode}", code)
        tengun_dict = copy.deepcopy(db)
        tengun_dict["url"] = url
        tengun_dict["dx"] = abs(tile.unitLength()[0])
        tengun_dict["dy"] = abs(tile.unitLength()[1])
        tengun_dict["meshcode"] = code
        tengun_dict["rect"] = tile.cellRect(code)[0]
        meshes.append(tengun_dict)
    
    urls = {}
    for i, tengun_dict in enumerate(meshes):
      urls[f"{i+1}/{len(meshes)}"] = {
        "url": tengun_dict["url"],
        "mesh_idx": i,
      }
    
    filesizes = self.UTIL.downloadHeadersConcurrently(args = urls)
    
    tengun_provide_areas = []
    for key, value in urls.items():
      if filesizes.get(key, -1) > 0:
        area_data = meshes[value["mesh_idx"]]
        area_data["filesize"] = filesizes[key]
        tengun_provide_areas.append(area_data)
          
    # initialize the sink
    (sink, dest_id) = self.parameterAsSink(
      parameters, "OUTPUT", context, 
      self.FIELDS["TENGUN_PROVIDED_AREAS"], QgsWkbTypes.Polygon, crs_output
    )
    
    # add features to the sink
    for area in tengun_provide_areas:
      ft = QgsFeature(self.FIELDS["TENGUN_PROVIDED_AREAS"])
      for k, v in area.items():
        if k in self.FIELDS["TENGUN_PROVIDED_AREAS"].names():
          ft[k] = v
          
      rect = QgsGeometry.fromRect(area["rect"])
      tf = QgsCoordinateTransform(area["rect"].crs(), crs_output, QgsProject.instance())
      rect.transform(tf)
      ft.setGeometry(rect)
        
      sink.addFeature(ft)
      
    return {"OUTPUT": dest_id}
  
  
  # Post processing; append layers
  def postProcessAlgorithm(self, context, feedback):
    return {}

  def createInstance(self):
    return fetchtengunprovideareas()
  
  def name(self):
    return 'fetchtengunprovideareas'
  
  def displayName(self):
    return self.tr("Tengun areas")

  
  # placing here is necessary, when employing pylupdate
  def tr(self, string):
    return QCoreApplication.translate(self.__class__.__name__, string)