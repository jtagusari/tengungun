from qgis.PyQt.QtCore import (QT_TRANSLATE_NOOP, QCoreApplication)
from qgis.core import (
  QgsProcessingParameterNumber,
  QgsProcessingParameterFeatureSource,
  QgsProcessing,
  QgsProcessingParameterField,
  QgsProcessingParameterFolderDestination,
  QgsFeatureRequest,
  QgsProcessingParameterBoolean,
  QgsCoordinateReferenceSystem,
  QgsProject,
  QgsProcessingAlgorithm
  )

import os
import glob
from .util.hriskutil import HrUtil
from qgis import processing

class fetchtengunusingareas(QgsProcessingAlgorithm):
  
  # UIs
  PARAMETERS = {  
    "PROVIDE_AREAS_LAYER": {
      "ui_func": QgsProcessingParameterFeatureSource,
      "ui_args":{
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","List of data to be fetched"),
        "types": [QgsProcessing.TypeVectorPolygon]
      }
    },
    "URL_FIELD": {
      "ui_func": QgsProcessingParameterField,
      "ui_args":{
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","URL Field of the data list layer"),
        "parentLayerParameterName": "PROVIDE_AREAS_LAYER",
        "defaultValue": "url"
      }
    },
    "CRS_FIELD": {
      "ui_func": QgsProcessingParameterField,
      "ui_args":{
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","CRS Field of the data list layer"),
        "parentLayerParameterName": "PROVIDE_AREAS_LAYER",
        "defaultValue": "crs"
      }
    },
    "MAX_DOWNLOAD": {
      "ui_func": QgsProcessingParameterNumber,
      "ui_args": {
        "type": QgsProcessingParameterNumber.Integer,
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","Maximum number of files to be downloaded"),
        "defaultValue": 10
      }
    },       
    "ADD_PC_TO_CURRENT_PROJ": {
      "ui_func": QgsProcessingParameterBoolean,
      "ui_args": {
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","Add point cloud(s) to the current project?"),
        "defaultValue": True
      }
    },       
    "OUTPUT": {
      "ui_func": QgsProcessingParameterFolderDestination,
      "ui_args": {
        "description": QT_TRANSLATE_NOOP("fetchtengunusingareas","Folder to output")
      }
    }
  }
      
  def __init__(self):
    super().__init__()
    self.UTIL = HrUtil(self)    
  
  # initialization of the algorithm
  def initAlgorithm(self, config):    
    self.UTIL.initParameters()
  
  
  # execution of the algorithm
  def processAlgorithm(self, parameters, context, feedback):        
    # import ptvsd
    # ptvsd.debug_this_thread()    
    
    # set processing parameters
    self.UTIL.registerProcessingParameters(parameters, context, feedback)
    
    # set output folder
    output_dir = os.path.normpath(self.parameterAsString(parameters, "OUTPUT", context))
    if len(output_dir) != len(output_dir.encode('utf-8')):
      feedback.reportError(self.tr("The output folder path must not contain multi-byte characters."))
      raise Exception(self.tr("The output folder path must not contain multi-byte characters."))
    if not os.path.exists(output_dir):
      os.makedirs(output_dir)
    
    # load inputs
    provide_areas = self.parameterAsSource(parameters, "PROVIDE_AREAS_LAYER", context).materialize(QgsFeatureRequest(), feedback)
    url_field = self.parameterAsString(parameters, "URL_FIELD", context)
    crs_field = self.parameterAsString(parameters, "CRS_FIELD", context)
    
    # set fetch arguments
    fetch_args = {}
    for i, ft in enumerate(provide_areas.getFeatures()):
      tng_crs = QgsCoordinateReferenceSystem(ft[crs_field]) if crs_field is not None else None
      if not tng_crs.isValid():
        raise Exception(self.tr("Invalid CRS"))
      
      fetch_args[f"{i+1}/{provide_areas.featureCount()}"] = {
        "url": ft[url_field],
        "crs": tng_crs
      }
    
    if len(fetch_args) > self.parameterAsInt(parameters, "MAX_DOWNLOAD", context):
      feedback.reportError(self.tr("Too many downloads are required: ") + str(len(fetch_args)))
      raise Exception(self.tr("Too many downloads are required: ") + str(len(fetch_args)))

    # download files
    fetch_results = self.UTIL.downloadFilesConcurrently(
      args = fetch_args, parallel=False
    )
    
    # extract and project the data
    tng_output = []
    for key, value in fetch_results.items():
      _, extracted_files = self.UTIL.extractArchive(value, pattern = "\\.(las|laz)$")
      feedback.pushInfo(self.tr("Extracted: ") + str(extracted_files))
      tng_crs = fetch_args[key]["crs"]
      
      for tng in extracted_files:
        dest_path = os.path.join(output_dir, os.path.basename(tng))
        tng_output.append(os.path.normpath(dest_path).replace(os.path.sep, "/"))
        processing.run(
          "pdal:assignprojection", 
          {
            "INPUT": tng,
            "CRS": tng_crs,
            "OUTPUT": dest_path
          },
          feedback = feedback,
          is_child_algorithm=True
        )["OUTPUT"]
        feedback.pushInfo(self.tr("Data was projected: ") + dest_path)
    
    # add to the current project
    if self.parameterAsBoolean(parameters, "ADD_PC_TO_CURRENT_PROJ", context):
      
      for tng in glob.glob(os.path.join(output_dir, "*")):
        context.addLayerToLoadOnCompletion(
          tng, 
          context.LayerDetails(os.path.splitext(os.path.basename(tng))[0], QgsProject.instance(), "")
        )
    
    return {"OUTPUT": tng_output}
  
  # Post processing; append layers
  def postProcessAlgorithm(self, context, feedback):
    return {}

  def displayName(self):
    return self.tr("Fetch data using Area Data")

  def createInstance(self):
    return fetchtengunusingareas()

  def name(self):
    return 'fetchtengunusingareas'
  
  
  # placing here is necessary, when employing pylupdate
  def tr(self, string):
    return QCoreApplication.translate(self.__class__.__name__, string)