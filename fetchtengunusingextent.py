from qgis.PyQt.QtCore import (QT_TRANSLATE_NOOP, QCoreApplication)
from qgis.core import (
  QgsCoordinateReferenceSystem,
  QgsProcessingParameterExtent,
  QgsProcessingParameterNumber,
  QgsProcessingParameterEnum,
  QgsProcessingParameterBoolean,
  QgsProcessingParameterFolderDestination,
  QgsProcessingAlgorithm
  )
from qgis import processing

from .util.hriskutil import HrUtil
from .util.tengunvar import TengunDatabase

class fetchtengunusingextent(QgsProcessingAlgorithm):
  
  # UIs
  PARAMETERS = {  
    "FETCH_EXTENT": {
      "ui_func": QgsProcessingParameterExtent,
      "ui_args":{
        "description": QT_TRANSLATE_NOOP("fetchtengunusingextent","Extent for fetching data")
      }
    },
    "MAX_DOWNLOAD": {
      "ui_func": QgsProcessingParameterNumber,
      "ui_args": {
        "type": QgsProcessingParameterNumber.Integer,
        "description": QT_TRANSLATE_NOOP("fetchtengunusingextent","Maximum number of files to be downloaded"),
        "defaultValue": 4
      }
    },       
    "PC_DATABASE": {
      "ui_func": QgsProcessingParameterEnum,
      "ui_args": {
        "options": [],
        "description": QT_TRANSLATE_NOOP("fetchtengunusingextent","Point cloud database"),
        "defaultValue": 0
      }
    },
    "ADD_PC_TO_CURRENT_PROJ": {
      "ui_func": QgsProcessingParameterBoolean,
      "ui_args": {
        "description": QT_TRANSLATE_NOOP("fetchtengunusingextent","Add point cloud(s) to the current project?"),
        "defaultValue": True
      }
    },       
    "OUTPUT_FOLDER": {
      "ui_func": QgsProcessingParameterFolderDestination,
      "ui_args": {
        "description": QT_TRANSLATE_NOOP("fetchtengunusingextent","Folder to output")
      }
    }
  }
    
  
  def __init__(self):
    super().__init__()
    self.UTIL = HrUtil(self)    
    
  # initialization of the algorithm
  def initAlgorithm(self, config):    
    (extent, target_crs) = self.UTIL.getExtentAndCrsUsingCanvas()
    self.UTIL.setDefaultValue("FETCH_EXTENT", extent)
    self.UTIL.initParameters()
    self.PARAMETERS["PC_DATABASE"]["ui_args"]["options"] = [item["description"] for item in TengunDatabase]
    self.PARAMETERS["PC_DATABASE"]["ui_args"]["defaultValue"] = list(range(len(TengunDatabase)))  
  
  # execution of the algorithm
  def processAlgorithm(self, parameters, context, feedback):        
    # import ptvsd
    # ptvsd.debug_this_thread()
    self.UTIL.registerProcessingParameters(parameters, context, feedback)
    
    target_crs = QgsCoordinateReferenceSystem(
      TengunDatabase[self.parameterAsEnum(parameters, "PC_DATABASE", context)]["crs"]
      )
    
    provide_areas = processing.run(
      "fetchtengunprovideareas", 
      {
        "FETCH_EXTENT": self.parameterAsExtent(parameters, "FETCH_EXTENT", context),
        "TARGET_CRS": target_crs,
        "PC_DATABASE": [self.parameterAsEnum(parameters, "PC_DATABASE", context)],
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context=context,
      feedback=feedback,
      is_child_algorithm=True
    )["OUTPUT"]
    
    provide_areas = context.getMapLayer(provide_areas)
    
    if provide_areas.featureCount() <= 0:
      feedback.reportError(self.tr("No data found in the area."))
      return None
    
    tng_path = processing.run(
      "fetchtengunusingareas", 
      {
        "PROVIDE_AREAS_LAYER": provide_areas,
        "URL_FIELD": "url",
        "CRS_FIELD": "crs",
        "MAX_DOWNLOAD": self.parameterAsInt(parameters, "MAX_DOWNLOAD", context),
        "ADD_PC_TO_CURRENT_PROJ": self.parameterAsBool(parameters, "ADD_PC_TO_CURRENT_PROJ", context),
        "PC_DATABASE": [self.parameterAsEnum(parameters, "PC_DATABASE", context)],
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context=context,
      feedback=feedback,
      is_child_algorithm=True
    )
    
    return tng_path
  
  # Post processing; append layers
  def postProcessAlgorithm(self, context, feedback):
    return {}

  def displayName(self):
    return self.tr("Fetch data using Extent")

  def createInstance(self):
    return fetchtengunusingextent()

  def name(self):
    return 'fetchtengunusingextent'  
  
  # placing here is necessary, when employing pylupdate
  def tr(self, string):
    return QCoreApplication.translate(self.__class__.__name__, string)