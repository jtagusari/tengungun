<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>crosssection</name>
    <message>
        <location filename="../crosssection.py" line="36"/>
        <source>Point cloud layer</source>
        <translation>点群レイヤ</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="51"/>
        <source>Line layer</source>
        <translation>線レイヤ</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="94"/>
        <source>Width of the line buffer</source>
        <translation>線のバッファ幅</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="124"/>
        <source>Crosssection points</source>
        <translation>鉛直断面への投影</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="380"/>
        <source>Crosssection</source>
        <translation>鉛直断面への投影</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="383"/>
        <source>Tools</source>
        <translation>点群ツール</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="43"/>
        <source>Attributes of point cloud layer</source>
        <translation>点群レイヤの属性</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="59"/>
        <source>X value of the origin of line layer</source>
        <translation>線レイヤの始点x座標</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="68"/>
        <source>Y value of the origin of line layer</source>
        <translation>線レイヤの始点y座標</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="77"/>
        <source>Primary key of line layer</source>
        <translation>線レイヤのプライマリー・キー</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="86"/>
        <source>Attributes of line layer</source>
        <translation>線レイヤの属性</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="102"/>
        <source>Maximum number of outputs</source>
        <translation>最大出力数</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="110"/>
        <source>Output as files (or layers)</source>
        <translation>ファイルとして出力（もしくはレイヤとして出力）</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="117"/>
        <source>Overwrite files?</source>
        <translation>ファイルの上書き</translation>
    </message>
</context>
<context>
    <name>crosssectionfiles</name>
    <message>
        <location filename="../crosssectionfiles.py" line="32"/>
        <source>Point cloud layer</source>
        <translation type="obsolete">点群レイヤ</translation>
    </message>
    <message>
        <location filename="../crosssectionfiles.py" line="40"/>
        <source>Line layer</source>
        <translation type="obsolete">線レイヤ</translation>
    </message>
    <message>
        <location filename="../crosssectionfiles.py" line="47"/>
        <source>Width of the line buffer</source>
        <translation type="obsolete">線のバッファ幅</translation>
    </message>
    <message>
        <location filename="../crosssectionfiles.py" line="55"/>
        <source>Maximum number of outputs</source>
        <translation type="obsolete">最大出力数</translation>
    </message>
    <message>
        <location filename="../crosssection.py" line="130"/>
        <source>Folder to output</source>
        <translation>出力フォルダ</translation>
    </message>
</context>
<context>
    <name>fetchabstract</name>
    <message>
        <location filename="../fetchabstract.py" line="58"/>
        <source>Download was failed.</source>
        <translation>ダウンロードは失敗しました。</translation>
    </message>
    <message>
        <location filename="../fetchabstract.py" line="83"/>
        <source>Point cloud file was fetched: </source>
        <translation>点群ファイルが取得されました: </translation>
    </message>
    <message>
        <location filename="../fetchabstract.py" line="99"/>
        <source>Unpacking archive ...</source>
        <translation>アーカイブを解凍しています・・・</translation>
    </message>
    <message>
        <location filename="../fetchabstract.py" line="143"/>
        <source>Login session is not given.</source>
        <translation>ログインセッションがありません。</translation>
    </message>
    <message>
        <location filename="../fetchabstract.py" line="144"/>
        <source>Login session is not given</source>
        <translation>ログインセッションがありません。</translation>
    </message>
    <message>
        <location filename="../fetchabstract.py" line="172"/>
        <source>Fetching was canceled.</source>
        <translation>情報取得はキャンセルされました。</translation>
    </message>
</context>
<context>
    <name>fetchtengun</name>
    <message>
        <location filename="../listtengun.py" line="57"/>
        <source>Point cloud database</source>
        <translation>点群データベース</translation>
    </message>
</context>
<context>
    <name>fetchtengunextent</name>
    <message>
        <location filename="../fetchtengunextent.py" line="24"/>
        <source>Extent for fetching data</source>
        <translation>情報取得範囲</translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="31"/>
        <source>Maximum number of files to be downloaded</source>
        <translation>点群ファイルの最大ダウンロード数</translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="39"/>
        <source>Point cloud database</source>
        <translation>点群データベース</translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="141"/>
        <source>Fetch Point Cloud using extent</source>
        <translation>点群の取得（範囲を指定）</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="59"/>
        <source>Add point cloud(s) to the current project?</source>
        <translation>取得した点群をこのプロジェクトに追加</translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="53"/>
        <source>Folder to output</source>
        <translation>出力フォルダ</translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="104"/>
        <source>Number of files exceeds: </source>
        <translation>ファイル数が超過しています: </translation>
    </message>
    <message>
        <location filename="../fetchtengunextent.py" line="118"/>
        <source>The output folder path must not contain multi-byte characters.</source>
        <translation>出力フォルダはマルチバイト文字を含んではいけません。</translation>
    </message>
</context>
<context>
    <name>fetchtengunlist</name>
    <message>
        <location filename="../fetchtengunlist.py" line="27"/>
        <source>List of data to be fetched</source>
        <translation>点群URLを含むレイヤ</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="34"/>
        <source>URL Field of the data list layer</source>
        <translation>URLフィールド</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="52"/>
        <source>Maximum number of files to be downloaded</source>
        <translation>点群ファイルの最大ダウンロード数</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="66"/>
        <source>Folder to output</source>
        <translation>出力フォルダ</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="132"/>
        <source>Fetch Point Cloud using list</source>
        <translation>点群の取得（URL属性を使用）</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="43"/>
        <source>CRS Field of the data list layer</source>
        <translation>参照座標系（CRS）フィールド</translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="97"/>
        <source>Number of files exceeds: </source>
        <translation>ファイル数が超過しています: </translation>
    </message>
    <message>
        <location filename="../fetchtengunlist.py" line="111"/>
        <source>The output folder path must not contain multi-byte characters.</source>
        <translation>出力フォルダはマルチバイト文字を含んではいけません。</translation>
    </message>
</context>
<context>
    <name>listtengun</name>
    <message>
        <location filename="../listtengun.py" line="35"/>
        <source>Extent for fetching data</source>
        <translation>情報取得範囲</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="42"/>
        <source>Area for fetching data</source>
        <translation>情報取得範囲（ポリゴン）</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="50"/>
        <source>CRS for the output of the data list</source>
        <translation>出力グリッドの座標系</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="65"/>
        <source>Point-cloud grid</source>
        <translation>点群データグリッド</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="190"/>
        <source>Failed to get a mesh code of </source>
        <translation>メッシュコード取得の失敗</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="190"/>
        <source>The extent may be out of the range.</source>
        <translation>指定範囲がデータ提供範囲を超過している可能性があります。</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="222"/>
        <source>URL must contain {pcode}</source>
        <translation>URLは{pcode}を含む必要があります。</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="250"/>
        <source>Failed to get a mesh code.</source>
        <translation>メッシュコード取得の失敗</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="310"/>
        <source>Download was failed.</source>
        <translation>ダウンロードは失敗しました。</translation>
    </message>
    <message>
        <location filename="../listtengun.py" line="388"/>
        <source>List Point Cloud Properties</source>
        <translation>点群属性の取得</translation>
    </message>
</context>
<context>
    <name>transectlines</name>
    <message>
        <location filename="../transectlines.py" line="28"/>
        <source>Line layer</source>
        <translation>線レイヤ</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="43"/>
        <source>Target CRS (Cartesian coordinates)</source>
        <translation>参照座標系（直交座標系）</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="49"/>
        <source>Interval of the transect lines</source>
        <translation>直交線の間隔</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="66"/>
        <source>Length of the transect lines</source>
        <translation>直交線の長さ</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="373"/>
        <source>Transect lines</source>
        <translation>直交線</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="109"/>
        <source>The Target CRS is NOT a Cartesian Coordinate System</source>
        <translation>参照座標系が直交座標系ではありません</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="376"/>
        <source>Tools</source>
        <translation>点群ツール</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="35"/>
        <source>ID field of the line layer</source>
        <translation>線レイヤのIDフィールド</translation>
    </message>
    <message>
        <location filename="../transectlines.py" line="58"/>
        <source>Max interval to reconstruct the original path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
