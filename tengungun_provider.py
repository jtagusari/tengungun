from qgis.core import QgsProcessingProvider

from .fetchtengunusingextent import fetchtengunusingextent
from .fetchtengunusingareas import fetchtengunusingareas
from .fetchtengunprovideareas import fetchtengunprovideareas


class tengungun_provider(QgsProcessingProvider):

    def __init__(self):        
        QgsProcessingProvider.__init__(self)

    def unload(self):
        pass

    def loadAlgorithms(self):
        self.addAlgorithm(fetchtengunusingextent())
        self.addAlgorithm(fetchtengunusingareas())
        self.addAlgorithm(fetchtengunprovideareas())

    def id(self):
        return 'tengungun'

    def name(self):
        return self.tr('TENGUNGUN')

    def icon(self):
        return QgsProcessingProvider.icon(self)

    def longName(self):
        return self.name()
