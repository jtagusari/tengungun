from qgis.PyQt.QtCore import (
  QCoreApplication,QVariant
  )
from qgis.PyQt.QtGui import QColor
from qgis.core import (
  QgsFields,
  QgsField
  )

import re

# case insensitive Fields
class HrFields(QgsFields):

  def append(self, field, *args, overwrite = False, **kwargs):
    new_field_name = field.name().lower()
    existing_field_names = map(lambda x: x.lower(), self.names())
    
    if new_field_name in existing_field_names and not overwrite:
      raise Exception(f"Field {field.name()} is already defined.")
    else:
      if new_field_name in existing_field_names:
        filt = list(filter(lambda x: x.lower() == new_field_name, self.names()))
        if len(filt) > 1:
          raise Exception(f"Current fields already contain duplicated name (case insensitive): {field.name().lower()}.")
        else:
          self.remove(filt[0])
      super().append(field, *args, **kwargs)
      
  @classmethod
  def fromQgsFieldList(cls, QgsFieldList:list[QgsField], overwrite = False, **kwargs):
    assert isinstance(QgsFieldList, list), "QgsFieldList must be a list."
    fields_tmp = cls()
    for fld in QgsFieldList:
      assert isinstance(fld, QgsField), "Each element of QgsFieldList must be a QgsField."
      fields_tmp.append(fld, overwrite=overwrite, **kwargs)
    return fields_tmp
  
  def setComments(self, comment, append = True):
    fields_tmp = HrFields()
    for fld in self:
      if append:
        fld.setComment(fld.comment() + " " + comment)
      else:
        fld.setComment(comment)
      fields_tmp.append(fld)
    return fields_tmp
  
  @staticmethod
  def checkCaseInsensitiveFields(fields):
    names = [fld.name().lower() for fld in fields]
    if len(names) != len(set(names)):
      raise Exception("Current fields contain duplicated names (case-insensitive).")
  
  @classmethod
  def concatenateHrFields(cls, QgsFieldsList:list[QgsFields],overwrite = False):
    fields_target = cls()
    for fields in QgsFieldsList:
      for fld in fields():
        fields_target.append(fld, overwrite = overwrite)
    return fields_target
  
  
  def concat(self, fields, overwrite = False, **kwargs):
    fields_tmp = HrFields()
    for fld in self:
      field_idx = fields.indexFromName(fld.name())
      if field_idx >= 0:
        if overwrite:
          fields_tmp.append(fields.field(field_idx), **kwargs)
        else:
          fields_tmp.append(fld, **kwargs)
        fields.remove(field_idx)
      else:        
        fields_tmp.append(fld, **kwargs)
    
    for fld in fields:
      fields_tmp.append(fld, **kwargs)
    return fields_tmp
  
  def toQgsFields(self, **kwargs):
    fields_tmp = QgsFields()
    for fld in self.fields:
      fields_tmp.append(fld, **kwargs)
    return fields_tmp
  
  @classmethod
  def fromQgsFields(cls, fields, **kwargs):
    fields_tmp = cls()
    for fld in fields:
      fields_tmp.append(fld, **kwargs)
    return fields_tmp
  
  
  @classmethod
  def fromDict(cls, fields_dict:dict):
    fields_tmp = cls()
    for key, value in fields_dict.items():
      if type(value) in [int, bool]:
        fields_tmp.append(QgsField(key, QVariant.Int))
      elif type(value) in [float]:
        fields_tmp.append(QgsField(key, QVariant.Double))
      elif type(value) in [str]:
        fields_tmp.append(QgsField(key, QVariant.String))
    return fields_tmp
  
  @staticmethod
  def getDefaultValues(fields):
    vals = []
    for fld in fields:
      try:
        val_str = re.search(r'default\s?:\s?([^;]+);', fld.comment()).group(1)
        if fld.type() == QVariant.Double:
          vals.append(float(val_str))
        elif fld.type() == QVariant.Int:
          vals.append(int(val_str))
        elif fld.type() == QVariant.String:
          vals.append(val_str)
      except:
        vals.append(None)
    return vals


  @staticmethod
  def getSpecifiedComments(fields, key):
    vals = []
    for fld in fields:
      try:
        val_str = re.search(key + r'\s?:\s?([^;]+);', fld.comment()).group(1)
        vals.append(val_str)
      except:
        vals.append(None)
    return vals

  def __repr__(self):
    return str([
      f"({fld.displayName()} (type: {fld.displayType()}, {fld.comment()})"
      for fld in self.toList()
    ])
