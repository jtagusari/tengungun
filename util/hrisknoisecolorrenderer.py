from qgis.PyQt.QtGui import QColor
from qgis.core import (
  QgsSingleBandPseudoColorRenderer,
  QgsRasterShader,
  QgsColorRampShader,
  QgsRasterBandStats,
  QgsFillSymbol,
  QgsGraduatedSymbolRenderer,
  QgsRendererRange,
  QgsClassificationRange,
  QgsMarkerSymbol,
  QgsFillSymbol,
  QgsLineSymbol,
  QgsWkbTypes
  )

import copy
import os
import json

with open(os.path.join(os.path.dirname(__file__), "color_themes.json"), "r") as f:
  ColorThemes = json.load(f)

try:
  ColorThemes["Default"] = copy.deepcopy(ColorThemes["Weninger color scheme"])
except:
  ColorThemes["Default"] = {
    "theme_ref": "NULL theme",
    "range_map":{
      "ALL": {"lower": -999, "upper": 999, "color": [160, 186, 191]}
    }
  }

class HrNoiseColorRenderer (object):
  COLOR_ARGS = {
      "coloring": "none",
      "theme_ref": None,
      "attribute": "LAEQ",
      "band_idx" : 1,
      "fill_color": QColor(0,0,0,0),
      "border_color": QColor(0,0,0,255),
      "border_width": 0.5,
      "value_map" : {"min": {"value": None, "color": QColor("#f7f7f7")}},
      "range_map" : {"default": {"lower": -999, "upper": 999, "color": QColor(0,0,0,255)}},
      "set_clip" : True,
      "opacity" : 0.5
    }
  
  # initialize instance variables
  def __init__(
    self, 
    color_args:dict = {"coloring": "none"}, 
    visibility:bool = True,
    set_min_to_zero:bool = False,
    **kwargs
    ):
    super().__init__()  
    
    self.COLOR_ARGS = copy.deepcopy(self.COLOR_ARGS)
    self.GROUP = None
    self.VISIBILITY = True
    
    color_args.update(kwargs)
    self.setColorArgs(**color_args)
    self.setVisivility(visibility)
    if set_min_to_zero:
      self.setMinValToZero()
    
  
  # visibility setter
  def setVisivility(self, visibility):
    self.VISIBILITY = visibility
    
  def getVisibility(self):
    return self.VISIBILITY
  
  # color arguments setter
  def setColorArgs(self, **kwargs):    
    # if theme is given, look for the theme in the color themes
    if "theme" in kwargs.keys():
      if kwargs.get("theme_json_path") is None:
        try:
          color_theme = copy.deepcopy(ColorThemes[kwargs["theme"]])
        except:
          raise Exception("Unknown theme: " + kwargs["theme"])
      else:
        try:
          with open(kwargs["theme_json_path"], "r") as f:
            color_theme = json.load(f)
        except:
          raise Exception("Error in reading the external theme: " + kwargs["theme_json_path"])
          
      
      for k, v in kwargs.items():
        if k != "theme":
          color_theme[k] = v
      
      self.setColorArgs(**color_theme)
      return None
    
    # if each argument is given, set the argument
    # parsing the colors and setting the opacity is necessary
    for k, v in kwargs.items():
      if isinstance(v, dict):
        for vk, vv in v.items():
          if "color" in vv.keys():
            if isinstance(vv["color"], str):
              v[vk]["color"] = QColor(vv["color"])
            elif isinstance(vv["color"], list):
              v[vk]["color"] = QColor(*vv["color"])
            elif not isinstance(vv["color"], QColor):
              raise Exception("Unknown color format: " + str(vv["color"]))
          if "opacity" in kwargs.keys() and kwargs["opacity"] is not None:
            try:
              v[vk]["color"].setAlphaF(kwargs["opacity"])
            except:
              raise Exception("Error in setting the opacity: " + str(kwargs["opacity"]))
          
      self.COLOR_ARGS[k] = v
  
  def getColorArgs(self):
    return self.COLOR_ARGS
  
  def getColoring(self):
    return self.COLOR_ARGS["coloring"]
  
  # the minimum value of the color range is set to zero
  def setMinValToZero(self):
    self.COLOR_ARGS["value_map"]["min"]["value"] = 0.0
    
  # the minimum and maximum values of the color range are set to the minimum and maximum values of the layer
  def setValRangeUsingStats(self, layer, only_if_unset = True):
    stats = layer.dataProvider().bandStatistics(self.COLOR_ARGS["band_idx"], QgsRasterBandStats.All) 
    if only_if_unset:
      if self.COLOR_ARGS["value_map"]["min"]["value"] is None:
        self.COLOR_ARGS["value_map"]["min"]["value"] = stats.minimumValue
      if self.COLOR_ARGS["value_map"]["max"]["value"] is None:
        self.COLOR_ARGS["value_map"]["max"]["value"] = stats.maximumValue
    else:
      self.COLOR_ARGS["value_map"]["min"]["value"] = stats.minimumValue
      self.COLOR_ARGS["value_map"]["max"]["value"] = stats.maximumValue

  # Single band pseudo color renderer for a raster layer
  def setSingleBandPseudoColorRenderer(self, layer):    
    if layer.__class__.__name__ != "QgsRasterLayer":
      raise Exception("Single band pseudo color renderer is only applicable to a raster layer")
    if "min" not in self.COLOR_ARGS["value_map"].keys() or "max" not in self.COLOR_ARGS["value_map"].keys():
      self.setColorRangeUsingTheme("Greys")
      
    if self.COLOR_ARGS["value_map"]["min"]["value"] is None or self.COLOR_ARGS["value_map"]["max"]["value"] is None:
      self.setValRangeUsingStats(layer)
    
    cr_fun = QgsColorRampShader(
      minimumValue = self.COLOR_ARGS["value_map"]["min"]["value"],
      maximumValue = self.COLOR_ARGS["value_map"]["max"]["value"]
    )
    cr_fun.setColorRampItemList(
      [
        QgsColorRampShader.ColorRampItem(v["value"], v["color"]) for v in self.COLOR_ARGS["value_map"].values()
      ]
    )
    cr_fun.setClip(self.COLOR_ARGS["set_clip"])
    shader = QgsRasterShader()
    shader.setRasterShaderFunction(cr_fun)
    lyr_renderer = QgsSingleBandPseudoColorRenderer(layer.dataProvider(), self.COLOR_ARGS["band_idx"], shader)
    layer.setRenderer(lyr_renderer)
    
  
  # graduated symbol renderer for a vector layer
  def setGraduatedSymbolRenderer(self, layer):
    if layer.__class__.__name__ != "QgsVectorLayer":
      raise Exception("Graduated symbol renderer is only applicable to a vector layer")
    
    # change symboller according to the wkb-type
    symb_fun = None
    if layer.wkbType() in [QgsWkbTypes.Point, QgsWkbTypes.PointZ]:
      symb_fun = QgsMarkerSymbol.createSimple
    elif layer.wkbType() == QgsWkbTypes.Polygon:
      symb_fun = QgsFillSymbol.createSimple
    elif layer.wkbType() == QgsWkbTypes.LineString:
      symb_fun = QgsLineSymbol.createSimple
    else:
      raise Exception("Unsupported geometry type: " + str(layer.wkbType())) 
    
    # set renderer and symbols
    try:
      renderer = QgsGraduatedSymbolRenderer(self.COLOR_ARGS["attribute"])
      for key, value in self.COLOR_ARGS["range_map"].items():
        renderer.addClassRange(
          QgsRendererRange(
            QgsClassificationRange(key, value.get("lower"), value.get("upper")), 
            symb_fun({"color":value.get("color")})
          )
        )
      # apply renderer
      layer.setRenderer(renderer)
    except:
      raise Exception("Error in setting the graduated symbol renderer")
      
      
  # use a single symbol renderer for a vector layer
  def setFillSymbolRenderer(self, layer):
    if layer.__class__.__name__ != "QgsVectorLayer":
      raise Exception("Single symbol renderer is only applicable to a vector layer")
    symbol = QgsFillSymbol.createSimple({"color":self.COLOR_ARGS["fill_color"], "color_border":self.COLOR_ARGS["border_color"], "width_border":self.COLOR_ARGS["border_width"]})
    layer.renderer().setSymbol(symbol)
    layer.setOpacity(self.COLOR_ARGS["opacity"])
  
  
  def applyRenderer(self, layer):
    # set renderer
    if self.COLOR_ARGS["coloring"] != "none":
      if self.COLOR_ARGS["coloring"] == "single_band_pseudo_color":
        try:
          self.setSingleBandPseudoColorRenderer(layer)
        except Exception as e:
          raise Exception("Error in setting the single band pseudo color renderer: " + str(e))
      elif self.COLOR_ARGS["coloring"] == "simple_bbox":
        try:
          self.setFillSymbolRenderer(layer)      
        except Exception as e:
          raise Exception("Error in setting the fill symbol renderer: " + str(e))
      elif self.COLOR_ARGS["coloring"] == "graduated_symbol":
        try:
          self.setGraduatedSymbolRenderer(layer)
        except Exception as e:
          raise Exception("Error in setting the graduated symbol renderer: " + str(e))
          
      layer.triggerRepaint()
