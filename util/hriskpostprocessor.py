from qgis.core import (
  QgsProject,
  QgsProcessingLayerPostProcessorInterface
  )

from util.hrisknoisecolorrenderer import HrNoiseColorRenderer


PostProcessors = {}

class HrPostProcessor (QgsProcessingLayerPostProcessorInterface):
  
  NCR = None
  GROUP = None
  HISTORY = None
  
  # initialize instance variables
  def __init__(self, history:list = [], group = None, **kwargs):
    super().__init__()
    self.HISTORY = []
    self.GROUP = None
    self.NCR = HrNoiseColorRenderer(**kwargs)
      
    self.setHistory(history)
    self.setGroup(group)
  
  def setColorArgs(self, **kwargs):
    self.NCR.setColorArgs(**kwargs)
  
  def setVisibility(self, **kwargs):
    self.NCR.setVisivility(**kwargs)
  
  def setMinValToZero(self):
    self.NCR.setMinValToZero()
  
  # clone the instance
  
  def clone(self):
    kwargs = {
      "history": self.HISTORY,
      "color_args": self.NCR.getColorArgs(),
      "group": self.GROUP,
      "visibility": self.NCR.getVisibility()
    }
    return HrPostProcessor(**kwargs)
  
  # group setter
  def setGroup(self, group):
    self.GROUP = group

  # history setter
  def setHistory(self, history:list):
    self.HISTORY = history

  # history getter
  def getHistory(self):
    return self.HISTORY
  
  # history extender
  def extendHistory(self, history:list):
    self.HISTORY.extend(history)
  
  # Post processing MAIN
  def postProcessLayer(self, layer, context, feedback):
    
    # set history items
    layer_meta = layer.metadata()
    if len(self.HISTORY) > 0:
      for history in self.HISTORY:
        layer_meta.addHistoryItem(history)
    layer.setMetadata(layer_meta)
    
    # set renderer
    try:
      self.NCR.applyRenderer(layer)
    except Exception as e:
      feedback.pushWarning(str(e))
    
    # set group
    if self.GROUP is not None:      
      root = QgsProject.instance().layerTreeRoot()
      
      grp = root.findGroup(self.GROUP)
      if grp is None:
        root.insertGroup(0, self.GROUP)
        grp = root.findGroup(self.GROUP)
        
      vl = root.findLayer(layer.id())
      vl.setItemVisibilityChecked(self.NCR.getVisibility())
      vl_clone = vl.clone()
      parent = vl.parent()
      grp.insertChildNode(0, vl_clone)
      parent.removeChildNode(vl)
      