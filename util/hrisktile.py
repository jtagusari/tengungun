from qgis.core import (
  QgsCoordinateReferenceSystem,
  QgsReferencedRectangle,
  QgsCoordinateTransform,
  QgsProject,
  QgsRectangle,
  )

import itertools
from typing import Callable, Union, Tuple
import numpy as np


class HrTile:
  
  X_ORIG = None
  Y_ORIG = None
  X_UNIT = None
  Y_UNIT = None
  CODING_FUNC = None #x, y to code
  REV_CODING_FUNC = None # code to xc, yc
  
  def __init__(
      self, 
      xunit: float, 
      yunit: float, 
      xorig: float, 
      yorig: float,
      coding_func: Callable[[float, float], str] = None, 
      rev_coding_func: Callable[[str], Union[float, float]] = None
  ):
    

    assert xunit is not None, "The xunit must be given."
    assert yunit is not None, "The yunit must be given."
    
    self.X_ORIG = xorig
    self.Y_ORIG = yorig
    self.X_UNIT = xunit
    self.Y_UNIT = yunit

    def coding(long:float, lat:float) -> str:
      xidx, yidx = self.cellXyIdx(long, lat)
      return f"{xidx}_{yidx}"

    if coding_func is None:
      self.CODING_FUNC = coding
    else:
      self.CODING_FUNC = coding_func
    
    def rev_coding(code:str) -> Union[float, float]:
      xy_idx = code.split("_")
      x, y = self.cellXyCenter(int(xy_idx[0]), int(xy_idx[1]))
      return x, y

    if rev_coding_func is None:
      self.REV_CODING_FUNC = rev_coding
    else:
      self.REV_CODING_FUNC = rev_coding_func

  #-------------------------------
  
  def unitLength(self) -> Union[float, float]:
    return (self.X_UNIT, self.Y_UNIT)
  
  def origin(self) -> Union[float, float]:
    return (self.X_ORIG, self.Y_ORIG)
  
  #-------------------------------
  
  def cellXyIdx(self, value, *args) -> list[Tuple[int, int]]:
    """
    Returns the cell indices (x, y) for a given value.
    
    Parameters:
    value (str, float): The value to convert to cell indices.
    args (float): The y value.
    
    Returns:
    list[Tuple[int, int]]: The mesh index for the given cell.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """
    if isinstance(value, str):
      return self._cellXyIdx_str(value)
    elif isinstance(value, float):
      if args:
        return self._cellXyIdx_float(value, args[0])
      else:
        raise TypeError("The float y value must be given.")
    else:
      raise TypeError(f"Unsupported type: {type(value).__name__}. Value must be either str or float.")
  
  def _cellXyIdx_float(self, x:float, y:float) -> list[Tuple[int, int]]:
    xidx = int((x - self.X_ORIG) / self.X_UNIT)
    yidx = int((y - self.Y_ORIG) / self.Y_UNIT)
    return [(xidx, yidx)]
  
  def _cellXyIdx_str(self, code:str) ->list[Tuple[int, int]]:
    x, y = self.REV_CODING_FUNC(code)
    return self.cellXyIdx(x, y)
  
  #-------------------------------
  
  def cellXyCenter(self, value, *args, **kwargs) -> list[tuple[float, float]]:
    """
    Returns the center coordinates (x, y) for a given cell index or extent.
    
    Parameters:
    value (str, int or float): The cell code, cell index (x and y) or extent to convert to center coordinates.
    args (float): The y value.
    kwargs: The keyword arguments.
    
    Returns:
    Tuple[float, float] or Tuple[Tuple(float), Tuple(float)]: The center coordinates of the cell.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """
    if isinstance(value, str):
      return self._cellXyCenter_str(value)
    elif isinstance(value, int):
      if len(args) == 1 and isinstance(args[0], int):
        return self._cellXyCenter_int(value, args[0])
      else:
        raise TypeError("The y value must be given.")
    elif isinstance(value, float):
      if args and isinstance(args[0], float):
        return self._cellXyCenter_float(value, args[0])
      else:
        raise TypeError("The y value must be given.")
    else: 
      raise TypeError(f"Unsupported type: {type(value).__name__}. Value must be either str, int or float.")
  

  def _cellXyCenter_float(self, x:float, y:float) -> list[tuple[float, float]]:
    xyidx = self._cellXyIdx_float(x, y)[0]
    return [(
      self.X_ORIG + self.X_UNIT * xyidx[0] + self.X_UNIT / 2, 
      self.Y_ORIG + self.Y_UNIT * xyidx[1] + self.Y_UNIT / 2
      )]
  
  def _cellXyCenter_int(self, xidx:int, yidx:int) -> list[tuple[float, float]]:
    return [(
      self.X_ORIG + self.X_UNIT * xidx + self.X_UNIT / 2, 
      self.Y_ORIG + self.Y_UNIT * yidx + self.Y_UNIT / 2
      )]
  
  def _cellXyCenter_str(self, code:str) -> list[tuple[float, float]]:
    return [self.REV_CODING_FUNC(code)]
  
  #-------------------------------
  
  def cellMeshCode(self, value, *args, **kwargs) -> list[str]:
    """
    Returns the mesh code for the given coordinates, cell indices, or extent.
    
    Parameters:
    value (int or float): The cell code, cell index (x and y) or extent to convert to center coordinates.
    kwargs (dict): The keyword arguments.
    
    Returns:
    list[str]: The mesh code for the given coordinates, cell indices, or extent.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """    
    if isinstance(value, int):
      if len(args) == 1 and isinstance(args[0], int):
        return self._cellMeshCode_int(value, args[0])
      else:
        raise TypeError("The y value must be given.")
    elif isinstance(value, float):
      if len(args) == 1 and isinstance(args[0], float):
        return self._cellMeshCode_float(value, args[0])
      else:
        raise TypeError("The y value must be given.")
    else: 
      raise TypeError("Unsupported type: value must be either int or float.")
  
  def _cellMeshCode_int(self, xidx:int, yidx:int) -> list[str]:
    xyc = self.cellXyCenter(xidx, yidx)[0]
    return [self.CODING_FUNC(xyc[0], xyc[1])]

  def _cellMeshCode_float(self, x:float, y:float) -> list[str]:
    return [self.CODING_FUNC(x, y)]
    
    
  #-------------------------------
    
  def cellRect(self, value, *args, **kwargs) -> list[dict]:    
    """
    Returns the mesh code for the given coordinates, cell indices, or extent.
    
    Parameters:
    value (str, int or float): The cell code, cell index (x and y) or extent to convert to center coordinates.
    kwargs (dict): The keyword arguments.
    
    Returns:
    list[dict] 
        
    Raises:
    TypeError: If the value is not of a supported type.
    """    
    
    xyc = self.cellXyCenter(value, *args, **kwargs)[0]
    return [{
      "xmin": xyc[0] - 0.5 * abs(self.X_UNIT), 
      "ymin": xyc[1] - 0.5 * abs(self.Y_UNIT), 
      "xmax": xyc[0] + 0.5 * abs(self.X_UNIT), 
      "ymax": xyc[1] + 0.5 * abs(self.Y_UNIT)
    }]
                

class HrQgsTile(HrTile):
  CRS = None  # Coordinate Reference System for the tile
  
  def __init__(
      self, 
      crs: QgsCoordinateReferenceSystem,
      xunit: float, 
      yunit: float, 
      xorig: float = None, 
      yorig: float = None,
      coding_func: Callable[[float, float], str] = None, 
      rev_coding_func: Callable[[str], Union[float, float]] = None
  ):
    
    assert xunit is not None, "The xunit must be given."
    assert yunit is not None, "The yunit must be given."
    
    self.CRS = crs
    
    if crs.isGeographic() and xorig is None and yorig is None:
      xorig = -180.0
      yorig = 90.0
      
    super().__init__(
      xunit = xunit, 
      yunit = yunit, 
      xorig = xorig, 
      yorig = yorig, 
      coding_func = coding_func, 
      rev_coding_func = rev_coding_func
      )  
  
  #-------------------------------
  
  def cellXyIdx(self, value, *args, **kwargs) -> list[Tuple[int, int]]:
    """
    Returns the cell indices (x, y) for a given value.
    
    Parameters:
    value (str, float, QgsReferencedRectangle): The value to convert to cell indices.
    args (float): The y value.
    kwargs (dict): The keyword arguments.
    
    Returns:
    list[Tuple[int, int]]: The mesh index for the given cell.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """
    if isinstance(value, QgsReferencedRectangle):
      extent_tr = self.transformExtent(value)
      xidx_edge = tuple(int((x - self.X_ORIG) / self.X_UNIT) for x in (extent_tr.xMinimum(), extent_tr.xMaximum()))
      yidx_edge = tuple(int((y - self.Y_ORIG) / self.Y_UNIT) for y in (extent_tr.yMinimum(), extent_tr.yMaximum()))
      
      xy_idxs = []
      if "only_edge" in kwargs and kwargs["only_edge"]:
        for xidx, yidx in itertools.product(xidx_edge, yidx_edge):
          xy_idxs.append((xidx, yidx))
      else:
        xidxs = range(min(xidx_edge), max(xidx_edge) + 1)
        yidxs = range(min(yidx_edge), max(yidx_edge) + 1)
        for xidx, yidx in itertools.product(xidxs, yidxs):
          xy_idxs.append((xidx, yidx))
      return xy_idxs
    
    else:
      return super().cellXyIdx(value, *args, **kwargs)
    
  #-------------------------------
  
  def cellXyCenter(self, value, *args, **kwargs) -> list[Tuple[float, float]]:
    """
    Returns the center coordinates (x, y) for a given cell index or extent.
    
    Parameters:
    value (str, int, float or QgsReferencedRectangle): The cell code, cell index (x and y) or extent to convert to center coordinates.
    args (float): The y value.
    kwargs: The keyword arguments.
    
    Returns:
    list[Tuple[float, float]]: The center coordinates of the cell.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """
    if isinstance(value, QgsReferencedRectangle):
      extent_tr = self.transformExtent(value)
      x_edge = tuple(self.X_ORIG + self.X_UNIT * int((x - self.X_ORIG) / self.X_UNIT) for x in (extent_tr.xMinimum(), extent_tr.xMaximum()))
      y_edge = tuple(self.Y_ORIG + self.Y_UNIT * int((y - self.Y_ORIG) / self.Y_UNIT) for y in (extent_tr.yMinimum(), extent_tr.yMaximum()))
      
      xys = []
      if "only_edge" in kwargs and kwargs["only_edge"]:      
        for xc, yc in itertools.product(x_edge, y_edge):
          xys.append((xc, yc))        
      else:
        xcs = np.arange(min(x_edge), max(x_edge) + 0.5 * abs(self.X_UNIT), abs(self.X_UNIT))
        ycs = np.arange(min(y_edge), max(y_edge) + 0.5 * abs(self.Y_UNIT), abs(self.Y_UNIT))
        for xc, yc in itertools.product(xcs, ycs):
          xys.append((xc, yc))
      return xys

    else:
      return super().cellXyCenter(value, *args, **kwargs)
    
  
  #-------------------------------
  
  def cellMeshCode(self, value, *args, **kwargs) -> list[str]:
    """
    Returns the mesh code for the given coordinates, cell indices, or extent.
    
    Parameters:
    value (str, int, float or QgsReferencedRectangle): The cell code, cell index (x and y) or extent to convert to center coordinates.
    kwargs (dict): The keyword arguments.
    
    Returns:
    list[str]: The mesh code for the given coordinates, cell indices, or extent.
    
    Raises:
    TypeError: If the value is not of a supported type.
    """    
    if isinstance(value, QgsReferencedRectangle):
      xys = self.cellXyCenter(value, **kwargs)
      return [self.cellMeshCode(xy[0], xy[1])[0] for xy in xys]
    else:
      return super().cellMeshCode(value, *args, **kwargs)
         
    
  #-------------------------------
    
  def cellRect(self, value, *args, **kwargs) -> list[QgsReferencedRectangle]:
    """
    Returns the mesh code for the given coordinates, cell indices, or extent.
    
    Parameters:
    value (str, float, int or QgsReferencedRectangle): The cell code, cell index (x and y) or extent to convert to center coordinates.
    kwargs (dict): The keyword arguments.
    
    Returns:
    list[QgsReferencedRectangle]
        
    Raises:
    TypeError: If the value is not of a supported type.
    """    
    if isinstance(value, QgsReferencedRectangle):
    
      xycs = self.cellXyCenter(value, only_edge = False)
      
      if "dissolve" in kwargs and kwargs["dissolve"]:
        xcs = [xyc[0] for xyc in xycs]
        ycs = [xyc[1] for xyc in xycs]
        rect_dicts = [{
          "xmin": min(xcs) - 0.5 * abs(self.X_UNIT), 
          "ymin": min(ycs) - 0.5 * abs(self.Y_UNIT), 
          "xmax": max(xcs) + 0.5 * abs(self.X_UNIT), 
          "ymax": max(ycs) + 0.5 * abs(self.Y_UNIT)
        }]
      else:
        rect_dicts = [{
          "xmin": xyc[0] - 0.5 * abs(self.X_UNIT), 
          "ymin": xyc[1] - 0.5 * abs(self.Y_UNIT), 
          "xmax": xyc[0] + 0.5 * abs(self.X_UNIT), 
          "ymax": xyc[1] + 0.5 * abs(self.Y_UNIT)
        } for xyc in xycs]

    else:
      rect_dicts = super().cellRect(value, *args, **kwargs)
    
    rects = [
      QgsReferencedRectangle(
        QgsRectangle(
          rect_dict["xmin"], 
          rect_dict["ymin"], 
          rect_dict["xmax"], 
          rect_dict["ymax"]
        ),
        self.CRS
      ) for rect_dict in rect_dicts
    ]
    return rects

  def transformExtent(self, extent:QgsReferencedRectangle) -> QgsReferencedRectangle:
    """
    Transforms the extent to the CRS of the tile.
    """
    transform = QgsCoordinateTransform(extent.crs(), self.CRS, QgsProject.instance())
    extent_tr = QgsReferencedRectangle(transform.transformBoundingBox(extent), self.CRS)
    return extent_tr
            

class WebMercatorTile(HrQgsTile):
  
  EQUATOR_LENGTH = 40075016.68557849
  def __init__(self, zoom:int):
    
    assert zoom is not None, "The zoom level must be given."
    assert zoom in range(0, 21), "The zoom level must be in the range of 0 to 20."
    
    self.CRS = QgsCoordinateReferenceSystem("EPSG:3857")
        
    super().__init__(
      crs = QgsCoordinateReferenceSystem("EPSG:3857"), 
      xunit = self.EQUATOR_LENGTH / 2 ** zoom, yunit = -self.EQUATOR_LENGTH / 2 ** zoom, 
      xorig = -self.EQUATOR_LENGTH / 2, yorig = self.EQUATOR_LENGTH / 2
    )

