from qgis.PyQt.QtCore import (
  QCoreApplication,QVariant
  )

from qgis.core import (
  QgsVectorLayer,
  QgsFeatureSink,
  QgsProcessingParameterDefinition, 
  QgsProcessingParameterNumber,
  QgsProcessingUtils,
  QgsVectorFileWriter,
  QgsCoordinateTransformContext,
  QgsFeatureRequest,
  QgsProcessingParameterString,
  QgsProcessingParameterBoolean,
  QgsCoordinateReferenceSystem,
  QgsProcessingFeedback,
  QgsProcessingContext,
  QgsReferencedRectangle,
  QgsCoordinateTransform,
  QgsProject,
  QgsFields,
  QgsField,
  QgsFeature,
  QgsProcessingParameterExtent
  )

import os
import asyncio
import re
import shutil
import datetime
import zipfile
import copy
import json
import concurrent
import requests
import platform

from qgis.utils import iface
from qgis import processing

class HrUtil(object):

  PARENT = None
  PARENT_PARAMETERS = None
  PARENT_CONTEXT = None
  PARENT_FEEDBACK = None
  
  def __init__(self, parent, parent_parameters:dict = {}, parent_context: QgsProcessingContext= None, parent_feedback: QgsProcessingFeedback = None) -> None:
    self.PARENT = parent
    self.PARENT_PARAMETERS = parent_parameters
    self.PARENT_CONTEXT = parent_context
    self.PARENT_FEEDBACK = parent_feedback
    self.SINK = None
    self.DEST_ID = None
  
  def registerProcessingParameters(self, parent_parameters:dict, parent_context: QgsProcessingContext, parent_feedback: QgsProcessingFeedback):
    self.PARENT_PARAMETERS = parent_parameters
    self.PARENT_CONTEXT = parent_context
    self.PARENT_FEEDBACK = parent_feedback
  
  @staticmethod
  def getHriskVersion(meta_path:str = os.path.join(os.path.dirname(__file__), "metadata.txt")) -> str:
    try:
      with open(meta_path) as hrisk_meta:
        version_match = re.search(r"version=(\S+)", hrisk_meta.read())
        return version_match.group(1)
    except:
      return "unknown"
  
  @staticmethod
  def getNoiseModellingVersion() -> str:
    try:
      nm_home = os.environ["NOISEMODELLING_HOME"]
    except:
      raise Exception("Environment variable of NOISEMODELLING_HOME is not set.")
    
    try:
      for jarfile in os.listdir(os.path.join(nm_home, "lib")):
        if jarfile.startswith("noisemodelling-jdbc-"):
          with zipfile.ZipFile(os.path.join(nm_home, "lib", jarfile), 'r') as jar:
            with jar.open('META-INF/MANIFEST.MF') as manifest:
              version_match = re.search(r"Bundle-Version: (\S+)", manifest.read().decode())
              return version_match.group(1)
    except:
      return "unknown"

  def parseCurrentProcess(self, with_nm:bool=False):
    
    caller = self.PARENT.__class__.__name__
    hrisk_version = self.getHriskVersion()

    
    hr = caller + " (" + "H-RISK v " + hrisk_version +" )"
    dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    if with_nm:        
      nm = "NoiseModelling v " + self.getNoiseModellingVersion()
      return hr + " with " + nm + " at " + dt 
    else:
      return hr + " at " + dt

  def setDefaultValue(self, key, value) -> None:
    assert hasattr(self.PARENT, "PARAMETERS"), "PARAMETERS is not defined in the class."
    self.PARENT.PARAMETERS[key]["ui_args"]["defaultValue"] = value

  def initParameters(self) -> None:

    assert hasattr(self.PARENT, "PARAMETERS"), "PARAMETERS attribute is not defined in the class."
    
    for key, value in self.PARENT.PARAMETERS.items():
      args = value.get("ui_args")
      args["name"] = key
      args["description"] = self.PARENT.tr(args["description"])
              
      ui = value.get("ui_func")(**args)
      
      if value.get("advanced") is not None and value.get("advanced") == True:
        ui.setFlags(QgsProcessingParameterDefinition.FlagAdvanced)
        
      self.PARENT.addParameter(ui)  

  @classmethod
  def checkCrsAsCartesian(cls, crs: QgsCoordinateReferenceSystem) -> None:
    assert crs.isValid(), "The CRS is not valid."
    
    if crs.isGeographic() or crs.authid() == "EPSG:3857":
      raise Exception(cls.tr("The Target CRS is NOT a Cartesian Coordinate System."))

  @classmethod
  def getExtentAndCrsUsingCanvas(cls, get_xy_coords:bool=True) -> tuple[str, QgsCoordinateReferenceSystem]:

    try:
      map_rectangle = iface.mapCanvas().extent()
      proj_crs = iface.mapCanvas().mapSettings().destinationCrs()
      
      use_rect = QgsReferencedRectangle(map_rectangle, proj_crs)

      use_crs = proj_crs
      if get_xy_coords:
        # if the CRS is geographic, input the long/lat to the getUtmCrs
        if proj_crs.isGeographic():
          use_crs = cls.getUtmCrs(map_rectangle.center().x(), map_rectangle.center().y())
        # if the CRS is web-mercator, transform long/lat coordinates and input to the getUtmCrs
        elif proj_crs.authid() == "EPSG:3857":
          transform = QgsCoordinateTransform(proj_crs, QgsCoordinateReferenceSystem("EPSG:4326"), QgsProject.instance())
          map_rectangle = transform.transformBoundingBox(map_rectangle)
          use_crs = cls.getUtmCrs(map_rectangle.center().x(), map_rectangle.center().y())
      
      use_extent = ",".join(
        list(map(lambda x: str(x), [
        use_rect.xMinimum(), use_rect.xMaximum(), use_rect.yMinimum(), use_rect.yMaximum()
        ]))
      ) + f"[{proj_crs.authid()}]"
      
      return use_extent, use_crs
    except:
      return "", QgsCoordinateReferenceSystem()

  @staticmethod
  def getUtmCrs(lng: float, lat: float) -> QgsCoordinateReferenceSystem:
    """
    Gets the CRS of the UTM using longitude and latitude.

    Parameters:
    - lng (float): The longitude.
    - lat (float): The latitude.

    Returns:
    QgsCoordinateReferenceSystem: The UTM CRS.

    Raises:
    None
    """
    epsg_code = 32600 + 100 * (lat < 0) + int((lng + 180) / 6) + 1
    crs = QgsCoordinateReferenceSystem(f'EPSG:{epsg_code}')
    assert crs.isValid(), "The CRS is not valid."
    return crs
  
  def dissolve(self, vl:QgsVectorLayer, field_names_drop:list[str] = ["layer","path"]) -> QgsVectorLayer:
    
    lyr_valid = processing.run(
      "qgis:checkvalidity",
      {
        "INPUT_LAYER": vl,
        "VALID_OUTPUT": "TEMPORARY_OUTPUT"
      },
      context = self.PARENT_CONTEXT,
      is_child_algorithm = True
    )["VALID_OUTPUT"]
    
    field_names_dissolve = [
      fld.name() 
      for fld in self.PARENT_CONTEXT.getMapLayer(lyr_valid).fields() 
      if fld.name() not in field_names_drop
    ]
  
    # Dissolve
    lyr_dissolve = processing.run(
      "native:dissolve", 
      {
        "INPUT": lyr_valid,
        "FIELD": field_names_dissolve,
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context = self.PARENT_CONTEXT,
      is_child_algorithm = True
    )["OUTPUT"]
    
    # Multipart to Single parts
    lyr_single = processing.run(
      "native:multiparttosingleparts", 
      {
        "INPUT": lyr_dissolve,
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context = self.PARENT_CONTEXT,
      is_child_algorithm = True
    )["OUTPUT"]
    
    return lyr_single
  
  def downloadFile(self, session = None, feedback_progress: bool = False, method:str = "GET", url:str = "", query:str = None, dest:str = None, stream:bool=True, **kwargs:dict) -> str:

    if session is None:
      session = requests.Session()
    try:
      if query is not None:
        response = session.request(method = method, url = url + query, stream = stream)
      else:
        response = session.request(method = method, url = url, stream = stream)
    except Exception as e:
      return str(e)
    # if download is successful
    if response.status_code == 200:
      file_size = int(response.headers.get('content-length', 0))
      if dest is None:
        try:
          dest = re.search(r'.*\.[^|]*', os.path.basename(url)).group(0)
        except:
          dest = "downloaded_file"
        
      temp_dir = self.createTempDir()
      file_path = os.path.join(temp_dir, dest)
      
      if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))

      # write downloaded file and show progress
      with open(file_path, 'wb') as file:
        for data in response.iter_content(chunk_size=1024):
          file.write(data)
          if feedback_progress:
            self.PARENT_FEEDBACK.setProgress(int((file.tell() / file_size) * 100))
      
      if feedback_progress:
        self.PARENT_FEEDBACK.setProgress(0)
      
      return file_path
      
    else:
      return Exception("The Link was missing or download was failed.")


  def downloadHeader(self, session = None, feedback_progress: bool = False, method:str = "GET", url:str = "", query:str = None, dest:str = None, stream:bool=True, **kwargs:dict) -> str:
    file_size = -1
    
    if session is None:
      session = requests.Session()
      
    try:
      if query is not None:
        response = session.request(method = method, url = url + query, stream = stream)
      else:
        response = session.request(method = method, url = url, stream = stream)
        
      if response.status_code == 200:
        file_size = int(response.headers.get('content-length', 0)) / 1e6
    except:
      pass
    
    # if download is successful
      
    return file_size


  def downloadHeadersConcurrently(self, session=None, args:dict={}, parallel = True, omit_null = True) -> dict:
    
    mx_wk = None if parallel else 1
    downloaded = {}
    
    if session is None:
      session = requests.Session()
    
    # fetch in parallel
    with concurrent.futures.ThreadPoolExecutor(max_workers=mx_wk) as executor:
      for key, data_size in zip(
        args.keys(),
        executor.map(
        lambda d_arg : self.downloadHeader(
          session, 
          feedback_progress = not parallel,
          **d_arg 
          ), 
        args.values()
        )
      ):
        if self.PARENT_FEEDBACK.isCanceled():
          self.PARENT_FEEDBACK.reportError(self.tr("Data download was canceled."))
          raise Exception(self.tr("Data download was canceled."))
        
        self.PARENT_FEEDBACK.pushInfo(f"({key}) Got info from: {args[key]['url']}")
        
        if omit_null:
          downloaded[key] = data_size
    
    return downloaded

  def downloadFilesConcurrently(self, session=None, args:dict={}, parallel = True, omit_null = True) -> dict:
    """Download files concurrently.

    Args:
        session (_type_, optional): download session. Defaults to None.
        args (dict, optional): download arguments passed to downloadFile. Defaults to {}.
        parallel (bool, optional): whether the download is parallely done. Defaults to True.
        omit_null (bool, optional): _description_. Defaults to True.

    Raises:
        Exception: Data download was canceled.

    Returns:
        dict: paths of the downloaded files
    """
    
    mx_wk = None if parallel else 1
    downloaded = {}
    
    if session is None:
      session = requests.Session()
    
    # fetch in parallel
    with concurrent.futures.ThreadPoolExecutor(max_workers=mx_wk) as executor:
      for key, data_path in zip(
        args.keys(),
        executor.map(
        lambda d_arg : self.downloadFile(
          session, 
          feedback_progress = not parallel,
          **d_arg 
          ), 
        args.values()
        )
      ):
        if self.PARENT_FEEDBACK.isCanceled():
          self.PARENT_FEEDBACK.reportError(self.tr("Data download was canceled."))
          raise Exception(self.tr("Data download was canceled."))
        
        self.PARENT_FEEDBACK.pushInfo(f"({key}) Downloaded from: {args[key]['url']}")
        
        if omit_null and isinstance(data_path, str):
          downloaded[key] = data_path
    
    return downloaded

  @classmethod
  def extractArchive(cls, archive_path: str, extension: str = "", extract_dir: str = None, pattern: str = None) -> list[str]:
    """Extract the archive file.

    Args:
        archive_path (str): path of the archive file
        extension (str, optional): explicit extension of the archive file. Defaults to "".
        extract_dir (str, optional): directory where the archive is extracted. Defaults to None.
        pattern (str, optional): pattern of the file path, only paths matches with which are returned. Defaults to None.

    Returns:
        list[str]: paths of the extracted files
    """

    if extract_dir is None:
      extract_dir = os.path.normpath(os.path.dirname(QgsProcessingUtils.generateTempFilename("")))
    
    if not os.path.exists(extract_dir):
      os.makedirs(extract_dir)      
    
    if len(extension) > 0 and not archive_path.endswith(extension):
      os.rename(archive_path, archive_path + extension)
      
    extracted_files = []
    extracted_dirs = []
    
    try:
      shutil.unpack_archive(archive_path, extract_dir)
      for root, dirs, files in os.walk(extract_dir):
        extracted_dirs.append(root)
        paths = [os.path.join(root, x) for x in files]
        if pattern is None:
          extracted_files += paths
        else:
          for path in paths:
            if re.search(pattern, path):
              extracted_files.append(path)
    except:
      pass # if the file is not an archive file, do nothing
    
    return extracted_dirs, extracted_files
  
  @staticmethod
  def createTempDir() -> str:
    """Create a temporary directory.

    Returns:
        str: path of the temporary directory
    """
    tmp_dir = os.path.normpath(os.path.dirname(QgsProcessingUtils.generateTempFilename("")))
    if not os.path.exists(tmp_dir):
      os.makedirs(tmp_dir)
    
    return tmp_dir
  
  def parseNoiseModellingArgs(self, args:dict, target_crs:QgsCoordinateReferenceSystem, wps_args:dict = {}, ui_parameters:dict = {}) -> dict:
    """Parse the arguments for the noise modelling.

    Args:
        args (dict): arguments for the noise modelling
        target_crs (QgsCoordinateReferenceSystem): target CRS
        wps_args (dict, optional): argiments for the wps_script. Defaults to {}.
        ui_parameters (dict, optional): parameters given by the UI. Defaults to {}.

    Raises:
        Exception: CRS is not the same among input features.

    Returns:
        dict: parsed arguments for the noise modelling
    """
    
    assert isinstance(args, dict), self.tr("The paths must be a dictionary.")
    assert "GROOVY_SCRIPT" in args, self.tr("Groovy script is not specified.")
    assert os.environ["NOISEMODELLING_HOME"], self.tr("Environment variable of NOISEMODELLING_HOME is not set.")
    
    tmp_dir = self.createTempDir()
    grv_dir = os.path.join(os.path.dirname(__file__), "noisemodelling","hriskscript")
    wps_path = os.path.join(os.path.dirname(__file__), "noisemodelling","bin","wps_scripts")
    
    nm = {}
    
    nm["HOME"] = os.environ["NOISEMODELLING_HOME"]
    nm["TEMP_DIR"] = tmp_dir
    
    for key, value in args.items():
      nm[key] = value.replace("%nmtmp%", tmp_dir).replace("%grvhome%", grv_dir)

    
    # note that the order of the dictionary is important!
    nm_wps = {
      "w": '"' + tmp_dir + '"', 
      "s": '"' + nm["GROOVY_SCRIPT"] + '"',
      "noiseModellingHome": '"' + os.path.normpath(nm["HOME"]) + '"',
      "exportDir": '"' + tmp_dir+ '"',
      "inputSRID": target_crs.authid().replace("EPSG:", ""),
    }
    
    # set other arguments
    if ui_parameters is not None:
      for key, value in ui_parameters.items():
        if value.get("n_mdl") is not None:
          if value.get("save_layer_get_path", False) is True:
            src = self.PARENT.parameterAsSource(self.PARENT_PARAMETERS, key, self.PARENT_CONTEXT)
            if src is None: continue
            if src.sourceCrs() != target_crs:
              self.PARENT_FEEDBACK.reportError(self.tr("CRS is not the same among input features."), fatalError=True)
              raise Exception(self.tr("CRS is not the same among input features."))
            vl = src.materialize(QgsFeatureRequest(), self.PARENT_FEEDBACK)
            vl_path = os.path.join(tmp_dir, key + ".geojson")
            self.saveVectorLayer(vl, vl_path)
            # register in the WPS_ARGS
            nm_wps[value.get("n_mdl")] = '"' + vl_path + '"'
          else:
            if value.get("ui_func") == QgsProcessingParameterString:
              value_input = '"' + self.PARENT.parameterAsString(self.PARENT_PARAMETERS, key, self.PARENT_CONTEXT) + '"'
            if value.get("ui_func") == QgsProcessingParameterBoolean:
              value_input = self.PARENT.parameterAsInt(self.PARENT_PARAMETERS, key, self.PARENT_CONTEXT)
            if value.get("ui_func") == QgsProcessingParameterNumber:
              if value.get("ui_args").get("type", QgsProcessingParameterNumber.Double) == QgsProcessingParameterNumber.Integer:
                value_input = self.PARENT.parameterAsInt(self.PARENT_PARAMETERS, key, self.PARENT_CONTEXT)
              else:
                value_input = self.PARENT.parameterAsDouble(self.PARENT_PARAMETERS, key, self.PARENT_CONTEXT)
              
            # register in the WPS_ARGS
            nm_wps[value.get("n_mdl")] = value_input
    nm_wps.update(wps_args)
    
    nm["WPS_ARGS"] = nm_wps
    
    pf = platform.system()
    if pf in ["Darwin","Linux"]:
      nm["CMD"] = '/bin/bash "' + wps_path + '"' + "".join([" -" + k + " " + str(v) for k, v in nm_wps.items()])
    elif pf == "Windows":    
      nm["CMD"] = '"' + wps_path + '"' + "".join([" -" + k + " " + str(v) for k, v in nm_wps.items()])
      
    return nm

    
  @staticmethod
  def saveVectorLayer(vector_layer: QgsVectorLayer, path: str) -> None:
    """Save the vector layer.

    Args:
        vector_layer (QgsVectorLayer): vector layer
        path (str): path to save the vector layer
    """
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = "GeoJSON"
    QgsVectorFileWriter.writeAsVectorFormatV3(
      vector_layer, path, QgsCoordinateTransformContext(), save_options
    )
  
  def execNoiseModellingCmd(self, cmd:str, temp_dir:str) -> None:
    """Execute the noise modelling command.

    Args:
        cmd (str): NoiseModelling command
        temp_dir (str): Temporary directory
    """
    loop = asyncio.new_event_loop()
    loop.run_until_complete(self.streamNoiseModellingCmd(cmd, temp_dir))
    loop.close()
    
  async def streamNoiseModellingCmd(self, cmd:str, temp_dir:str) -> None:
    """Stream the noise modelling command.

    Args:
        cmd (str): NoiseModelling command
        temp_dir (str): Temporary directory

    Raises:
        Exception: NoiseModelling script was not successfully executed.
    """
    proc = await asyncio.create_subprocess_shell(
      cmd,
      stdout = asyncio.subprocess.PIPE,
      stderr = asyncio.subprocess.PIPE,
      cwd    = temp_dir
    )

    while True:
      if proc.stdout.at_eof() or proc.stderr.at_eof():
        break

      stderr_raw = await proc.stderr.readline()  # for debugging
      try:
        stderr = stderr_raw.decode("utf-8","ignore")
      except:
        stderr = ""
        self.PARENT_FEEDBACK.reportError(self.tr("NoiseModelling script was not successfully executed."), fatalError=True)
        raise Exception(self.tr("NoiseModelling script was not successfully executed."))

      if stderr:
        self.PARENT_FEEDBACK.pushConsoleInfo(stderr.replace("\n", ""))

      prg_match = re.search(r".*[0-9]+\.[0-9]+.*%", stderr)
      if prg_match:
        self.PARENT_FEEDBACK.setProgress(
          int(float(re.search(r"[0-9]+\.[0-9]+", prg_match.group()).group()))
        )
  
  @staticmethod
  def newFieldsWithHistory(current_fields: QgsFields) -> QgsFields:
    """Creates new fields with history.

    Args:
        current_fields (QgsFields): current fields

    Returns:
        QgsFields: new fields with history
    """
    # note that one-by-one appending is necessary to remove unsupported field type
    new_fields = QgsFields()
    for fld in current_fields:
      if fld.type() in [QVariant.Int, QVariant.Double, QVariant.String]:
        new_fields.append(fld)
      else:
        new_fields.append(QgsField(fld.name(), QVariant.String))
        
    if "HISTORY" not in [fld.name() for fld in current_fields]:
      new_fields.append(QgsField("HISTORY", QVariant.String))
    return new_fields
  
  @staticmethod
  def addFeaturesWithHistoryToSink(
    sink:QgsFeatureSink, vector_layer:QgsVectorLayer, fields:QgsFields, current_process:str,
    additional_attributes:dict = {}
    ) -> None:
    """Add features with history to the sink.

    Args:
        sink (QgsFeatureSink): sink of the features
        vector_layer (QgsVectorLayer): layer of the features
        fields (QgsFields): fields of the features
        current_process (str): current process, in string
        additional_attributes (dict, optional): additional attributes. Defaults to {}.

    Raises:
        Exception: Length of the additional attribute is not matched with the output.
    """
    
    for i, ft in enumerate(vector_layer.getFeatures()):
      new_ft = QgsFeature(fields)
      new_ft.setGeometry(ft.geometry())
      
      for fld in fields:
        if fld.name() != "HISTORY":
          if fld.name() in additional_attributes.keys():
            try:
              new_ft[fld.name()] = additional_attributes[fld.name()][i]
            except:
              raise Exception(f"Length of the additional attribute {fld.name} is not matched with the output.")
          elif fld.name() in ft.fields().names():
            new_ft[fld.name()] = ft[fld.name()]
        else:
          if "HISTORY" in ft.fields().names() and len(ft["HISTORY"]) > 0:
            new_ft["HISTORY"] = ft["HISTORY"] + "; " + current_process
          else:
            new_ft["HISTORY"] = current_process
      
      sink.addFeature(new_ft)
  
  @classmethod
  def fenceExtentAsLayer(cls, context:QgsProcessingContext, fence_extent:QgsProcessingParameterExtent, target_crs:QgsCoordinateReferenceSystem) -> QgsVectorLayer:
    """Converts the extent to a layer.

    Args:
        context (QgsProcessingContext): context of the processing
        fence_extent (QgsProcessingParameterExtent): extent of the fence
        target_crs (QgsCoordinateReferenceSystem): coordinate reference system

    Returns:
        QgsVectorLayer: Layer of the extent
    """
    fence_layer = processing.run(
      "native:extenttolayer",
      {
        "INPUT": fence_extent,
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context = context,
      is_child_algorithm = True
    )["OUTPUT"]
    
    fence_transformed = processing.run(
      "native:reprojectlayer",
      {
        "INPUT": fence_layer,
        "TARGET_CRS": target_crs,
        "OUTPUT": "TEMPORARY_OUTPUT"
      },
      context = context,
      is_child_algorithm = True
    )["OUTPUT"]
    
    return fence_transformed

  @classmethod
  def fenceExtentAsWkt(cls, context:QgsProcessingContext, fence_extent:QgsProcessingParameterExtent, target_crs:QgsCoordinateReferenceSystem) -> str:
    """Converts the extent to WKT.

    Args:
        context (QgsProcessingContext): context of the processing
        fence_extent (QgsProcessingParameterExtent): extent of the fence
        target_crs (QgsCoordinateReferenceSystem): coordinate reference system

    Returns:
        str: WKT of the extent
    """
    fence_layer = cls.fenceExtentAsLayer(context, fence_extent, QgsCoordinateReferenceSystem("EPSG:4326"))      
    fence_layer = context.getMapLayer(fence_layer)
    return fence_layer.getFeature(1).geometry().asWkt()

  def parseCrs(self, check_cartesian:bool = True) -> QgsCoordinateReferenceSystem:
    """Parses the CRS from the parameters of the PARENT instance.

    Args:
        check_cartesian (bool, optional): Check if the result is cartesian coordinates. Defaults to True.

    Returns:
        QgsCoordinateReferenceSystem: The CRS of the target.
    """
    if "TARGET_CRS" in self.PARENT_PARAMETERS.keys():
      target_crs = self.PARENT.parameterAsCrs(self.PARENT_PARAMETERS, "TARGET_CRS", self.PARENT_CONTEXT)
    else:
      crs_key = [key for key, value in self.PARENT.PARAMETERS.items() if value.get("crs_referrence") is not None and value.get("crs_referrence") == True]
      target_crs = self.PARENT.parameterAsSource(self.PARENT_PARAMETERS, crs_key[0], self.PARENT_CONTEXT).sourceCrs()
    
    if check_cartesian:
      self.checkCrsAsCartesian(target_crs)
    return target_crs
  
  
  def tr(self, string:str):
    return QCoreApplication.translate(self.__class__.__name__, string)
      
  # Post processing; append layers
  @staticmethod
  def registerPostProcessAlgorithm(context: QgsProcessingContext, postprocessors: dict) -> dict:
    """Registers the post-processors to the layers.

    Args:
        context (QgsProcessingContext): context of the processing
        postprocessors (dict): maste dictionary of postprocessors

    Returns:
        dict: {}
    """
    
    for lyr_id, _ in context.layersToLoadOnCompletion().items():
      if lyr_id in postprocessors.keys():
        context.layerToLoadOnCompletionDetails(lyr_id).setPostProcessor(postprocessors[lyr_id])
    return {}
  
  @staticmethod
  def getColorThemes(path:str = os.path.join(os.path.dirname(__file__), "color_themes.json")) -> dict:
    """Reads the color themes from the json file.

    Args:
        path (str, optional): Path of the json file of the colors. Defaults to os.path.join(os.path.dirname(__file__), "color_themes.json").

    Returns:
        dict: Dictionary of the color themes
    """
    with open(path, "r") as f:
      color_themes = json.load(f)
    
    return color_themes
