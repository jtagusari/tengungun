from qgis.PyQt.QtCore import (QSettings)

import os
import json

with open(os.path.join(os.path.dirname(__file__),"pc-databases.json")) as f:
  TengunDatabase = json.load(f)
  
locale = QSettings().value("locale/userLocale", "en", type=str)[0:2]
if locale == "ja":
  TengunDatabase = [
    {**db_info, **{"description": db_info["description_ja"]}} 
    for db_info in TengunDatabase
  ]