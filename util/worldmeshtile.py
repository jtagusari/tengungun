from qgis.core import (
  QgsCoordinateReferenceSystem
  )

from typing import Union, Tuple
from .hrisktile import HrQgsTile

class WorldMeshTile(HrQgsTile):
  
  ORIG = {"x": 0.0, "y": 0.0}
  
  MESH_UNIT = {
    "1":         {"long": 1.0,          "lat": 40.0/60.0},
    "2":         {"long": 1.0/8.0,      "lat": 40.0/60.0/8.0},
    "3":         {"long": 1.0/80.0,     "lat": 40.0/60.0/80.0},
    "4":         {"long": 1.0/160.0,    "lat": 40.0/60.0/160.0},
    "5":         {"long": 1.0/320.0,    "lat": 40.0/60.0/320.0},
    "6":         {"long": 1.0/640.0,    "lat": 40.0/60.0/640.0},
    "ex100m_12": {"long": 4.5/3600.0,   "lat": 3.0/3600.0},
    "ex100m_13": {"long": 4.5/3600.0,   "lat": 3.0/3600.0},
    "ex50m_13":  {"long": 2.25/3600.0,  "lat": 1.5/3600.0},
    "ex50m_14":  {"long": 2.25/3600.0,  "lat": 1.5/3600.0},
    "ex10m_14":  {"long": 0.45/3600.0,  "lat": 0.3/3600.0},
    "ex1m_16":   {"long": 0.045/3600.0, "lat": 0.03/3600.0},
  }
  
  MESH_UNIT["1"]["fmt"] = "{code0}{y_1}{x_1}"
  MESH_UNIT["2"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}"
  MESH_UNIT["3"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}"
  MESH_UNIT["4"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{xy_4}"
  MESH_UNIT["5"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{xy_4}{xy_5}"
  MESH_UNIT["6"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{xy_4}{xy_5}{xy_6}"
  MESH_UNIT["ex100m_12"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{y_ex100m_12}{x_ex100m_12}"
  MESH_UNIT["ex100m_13"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{xy_4}{y_ex100m_13}{x_ex100m_13}"
  MESH_UNIT["ex50m_14"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{xy_4}{y_ex100m_13}{x_ex100m_13}{xy_ex50m_14}"
  MESH_UNIT["ex10m_14"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{y_ex100m_12}{x_ex100m_12}{y_ex10m_14}{x_ex10m_14}"
  MESH_UNIT["ex1m_16"]["fmt"] = "{code0}{y_1}{x_1}{y_2}{x_2}{y_3}{x_3}{y_ex100m_12}{x_ex100m_12}{y_ex50m_13}{x_ex50m_13}{y_ex10m_14}{x_ex10m_14}{y_ex1m_16}{x_ex1m_16}"
  
  MESH_UNIT["1"]["keys_use"] = ["1"]
  MESH_UNIT["2"]["keys_use"] = ["1", "2"]
  MESH_UNIT["3"]["keys_use"] = ["1", "2", "3"]
  MESH_UNIT["4"]["keys_use"] = ["1", "2", "3", "4"]
  MESH_UNIT["5"]["keys_use"] = ["1", "2", "3", "4", "5"]
  MESH_UNIT["6"]["keys_use"] = ["1", "2", "3", "4", "5", "6"]
  MESH_UNIT["ex100m_12"]["keys_use"] = ["1", "2", "3", "ex100m_12"]
  MESH_UNIT["ex100m_13"]["keys_use"] = ["1", "2", "3", "4", "ex100m_13"]
  MESH_UNIT["ex50m_13"]["keys_use"] = ["1", "2", "3", "ex100m_12", "ex50m_13"]
  MESH_UNIT["ex50m_14"]["keys_use"] = ["1", "2", "3", "4", "ex100m_13", "ex50m_14"]
  MESH_UNIT["ex10m_14"]["keys_use"] = ["1", "2", "3", "ex100m_12", "ex10m_14"]
  MESH_UNIT["ex1m_16"]["keys_use"] = ["1", "2", "3", "ex100m_12", "ex50m_13", "ex10m_14", "ex1m_16"]
  
  LEVEL = None
  
  @classmethod
  def dxdy(cls, level="1") -> Tuple[float, float]:
    assert level in cls.MESH_UNIT.keys(), "The level must be one of the following: " + ", ".join(cls.MESH_UNIT.keys())
    return (cls.MESH_UNIT[level]["long"], cls.MESH_UNIT[level]["lat"]) 
  
  def _xyToCode(self, x:float, y:float, level:str = "1") -> str:
    assert level in self.MESH_UNIT.keys(), "The level must be one of the following: " + ", ".join(self.MESH_UNIT.keys())
    assert y >= -90.0 and y <= 90.0, "The latitude must be within -90 to 90."
    
    x += 360.0 * int((180.0 - x) / 360.0)
    xy_idx = self.cellXyIdx(x, y)[0]
        
    x_scales = {
      k: int(v["long"] / self.MESH_UNIT[level]["long"]) 
      for k, v in self.MESH_UNIT.items()
      if k in self.MESH_UNIT[level]["keys_use"]
    }
    
    y_scales = {
      k: int(v["lat"] / self.MESH_UNIT[level]["lat"]) 
      for k, v in self.MESH_UNIT.items()
      if k in self.MESH_UNIT[level]["keys_use"]
    }
    
    idxs = {}
    
    idxs["code0"] = 10
    if x >= 100:
      idxs["code0"] += 10
      idxs["x_1"] -= 100
    elif x < 0:
      idxs["code0"] += 20
    elif x < -100:
      idxs["code0"] += 30
      idxs["x_1"] += 100
    if y < 0:
      idxs["code0"] += 40
      
    x_idx_r, y_idx_r = abs(xy_idx[0]), abs(xy_idx[0])
    for k, v in x_scales.items():
      idxs["x_" + k] = x_idx_r // v
      x_idx_r = x_idx_r % v
    for k, v in y_scales.items():
      idxs["y_" + k] = y_idx_r // v
      y_idx_r = y_idx_r % v   
    
    if "4" in self.MESH_UNIT[level]["keys_use"]:
      idxs["xy_4"] = 1 + idxs["x_4"] + 2 * idxs["y_4"]
    
    if "5" in self.MESH_UNIT[level]["keys_use"]:
      idxs["xy_5"] = 1 + idxs["x_5"] + 2 * idxs["y_5"]
    
    if "6" in self.MESH_UNIT[level]["keys_use"]:
      idxs["xy_6"] = 1 + idxs["x_6"] + 2 * idxs["y_6"]
    
    if "ex50m_13" in self.MESH_UNIT[level]["keys_use"]:
      idxs["xy_ex50m_13"] = 1 + idxs["x_ex50m_13"] + 2 * idxs["y_ex50m_13"]
    
    if "ex50m_14" in self.MESH_UNIT[level]["keys_use"]:
      idxs["xy_ex50m_14"] = 1 + idxs["x_ex50m_14"] + 2 * idxs["y_ex50m_14"]
      
    
    return self.MESH_UNIT[level]["fmt"].format(**idxs)
  
  def _codeToXy(self, code:str, level:str = None) -> Tuple[float, float]:
      
      assert len(code >= 6), "The mesh code must be longer than 6 characters."
      
      x_sgn = 1 - 2 * ((int(code[0]) - 1) % 4) // 2
      y_sgn = 1 - 2 * ((int(code[0]) - 1) // 4)
      x_offset = 0 if int(code[0]) % 2 == 1 else 100
      
      xc, yc = self.origin()
      if "1" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("1")
        xc += dx * int(code[4:6])
        yc += dy * int(code[2:4])
      if "2" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("2")
        xc += dx * int(code[7])
        yc += dy * int(code[6])
      if "3" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("3")
        xc += dx * int(code[9])
        yc += dy * int(code[8])
      if "4" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("4")
        xc += dx * ((int(code[10]) - 1) % 2)
        yc += dy * ((int(code[10]) - 1) // 2)
      if "5" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("5")
        xc += dx * ((int(code[11]) - 1) % 2)
        yc += dy * ((int(code[11]) - 1) // 2)
      if "6" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("6")
        xc += dx * ((int(code[12]) - 1) % 2)
        yc += dy * ((int(code[12]) - 1) // 2)
      if "ex100m_12" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex100m_12")
        xc += dx * int(code[13])
        yc += dy * int(code[12])
      if "ex100m_13" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex100m_13")
        xc += dx * int(code[14])
        yc += dy * int(code[13])
      if "ex50m_13" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex50m_13")
        xc += dx * ((int(code[14]) - 1) % 2)
        yc += dy * ((int(code[14]) - 1) // 2)
      if "ex50m_14" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex50m_14")
        xc += dx * ((int(code[15]) - 1) % 2)
        yc += dy * ((int(code[15]) - 1) // 2)
      if "ex10m_14" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex10m_14")
        xc += dx * int(code[15])
        yc += dy * int(code[14])
      if "ex1m_16" in self.MESH_UNIT[level]["keys_use"]:
        dx, dy = self.dxdy("ex1m_16")
        xc += dx * int(code[17])
        yc += dy * int(code[16])
      
      dx, dy = self.dxdy(level)

      return (x_sgn * (xc + dx * 0.5 + x_offset), y_sgn * (yc + dy * 0.5))
                
  def __init__(self, level:str, crs:QgsCoordinateReferenceSystem=QgsCoordinateReferenceSystem("EPSG:4326")):
    assert level in self.MESH_UNIT.keys(), "The worldmesh level is not valid. It must be one of the following: " + ", ".join(self.MESH_UNIT.keys())
        
    self.X_ORIG = self.ORIG["x"]
    self.Y_ORIG = self.ORIG["y"]
    
    assert crs.isGeographic(), "The CRS must be geographic."
    
    self.LEVEL = level
    dx, dy = self.dxdy(level)    
    
    def worldmesh_coding(x:float, y:float) -> str:
      return self._xyToCode(x, y, level)
  
    def worldmesh_rev_coding(code:str) -> Union[float, float]:
      return self._codeToXy(code, level)
    
    super().__init__(
      crs = crs, 
      xunit = dx, yunit = -dy, 
      xorig = self.X_ORIG, yorig = self.Y_ORIG, 
      coding_func = worldmesh_coding, rev_coding_func = worldmesh_rev_coding
    )
