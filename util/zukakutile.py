from qgis.core import (
  QgsCoordinateReferenceSystem
  )

from .hrisktile import HrQgsTile


from typing import Union, Tuple

class ZukakuTile(HrQgsTile):
  COORDS_JA_ALL = {
    "6669": "01", "6670": "02", "6671": "03", "6672": "04", "6673": "05", "6674": "06", "6675": "07", "6676": "08", "6677": "09", "6678": "10", "6679": "11", "6680": "12", "6681": "13", "6682": "14", "6683": "15", "6684": "16", "6685": "17", "6686": "18", "6687": "19",
    "2443": "01", "2444": "02", "2445": "03", "2446": "04", "2447": "05", "2448": "06", "2449": "07", "2450": "08", "2451": "09", "2452": "10", "2453": "11", "2454": "12", "2455": "13", "2456": "14", "2457": "15", "2458": "16", "2459": "17", "2460": "18", "2461": "19",
  }
  
  ORIG = {"x": -160.0e3, "y": 300.0e3}
  
  MESH_UNIT = {
    "50000": {"scale": 50000, "dx": 40.0e3, "dy": -30.0e3},
    "5000":  {"scale":  5000, "dx": 4.0e3,  "dy":  -3.0e3},
    "2500":  {"scale":  2500, "dx": 2.0e3,  "dy":  -1.5e3},
    "1000":  {"scale":  1000, "dx": 0.8e3,  "dy":  -0.6e3},
    "500":   {"scale":   500, "dx": 0.4e3,  "dy":  -0.3e3}
  }
  
  MESH_UNIT["50000"]["fmt"] = "{code_50000}"
  MESH_UNIT["5000"]["fmt"]  = "{code_50000}{y_5000}{x_5000}"
  MESH_UNIT["2500"]["fmt"]  = "{code_50000}{y_5000}{x_5000}{xy_2500}"
  MESH_UNIT["1000"]["fmt"]  = "{code_50000}{y_5000}{x_5000}{y_1000}{x_1000}"
  MESH_UNIT["500"]["fmt"]   = "{code_50000}{y_5000}{x_5000}{y_500}{x_500}"
  
  MESH_UNIT["50000"]["keys_use"] = ["50000"]
  MESH_UNIT["5000"]["keys_use"]  = ["50000", "5000"]
  MESH_UNIT["2500"]["keys_use"]  = ["50000", "5000", "2500"]
  MESH_UNIT["1000"]["keys_use"]  = ["50000", "5000", "1000"]
  MESH_UNIT["500"]["keys_use"]   = ["50000", "5000", "500"]
  
  CRS_CODE = None
  LEVEL = None
  
  @classmethod
  def dxdy(cls, level="1") -> Tuple[float, float]:
    assert level in cls.MESH_UNIT.keys(), "The level must be one of the following: " + ", ".join(cls.MESH_UNIT.keys())
    return (cls.MESH_UNIT[level]["dx"], cls.MESH_UNIT[level]["dy"]) 
  
  def _checkZukakuCode(self, code:str, level:str) -> None:
      
    # conditions that must be satisfied for all codes
    assert level in self.MESH_UNIT.keys(), "Level must be specified from: " + self.MESH_UNIT.keys()
    assert len(code) >= 4, "Code must be longer than 4 characters"
    
    try:
      crs_code = int(code[0:2])
      if crs_code > 19:
        raise Exception
    except:
      raise ValueError("The first two characters of Code must be within 01-19")
    
    try:
      xc = code[3].upper()
      yc = code[2].upper()
      if ord(xc) < ord("A") or ord(xc) > ord("T") or ord(yc) < ord("A") or ord(yc) > ord("T"):
        raise Exception
    except:
      raise ValueError("The upper cases of Code[2] and Code[3] must be within A-T and A-H")
    
    # if the level is 50000, return OK at this point
    if level == "50000":
      return True

    # conditions that must be satisfied for 5000, 2500, 1000, 500    
    assert len(code) >= 6, "Code must be longer than 6 characters"
    try:
      _ = int(code[5])
      _ = int(code[4])
    except:
      raise ValueError("Code[4] and Code[5] must be within 0-9")
       
    # if the level is 5000, return OK at this point
    if level == "5000":
      return True
    
    # conditions for 2500
    if level == "2500":
      assert len(code) >= 7, "Code must be longer than 7 characters"
      try:
        nxy3 = int(code[6]) 
        if nxy3 < 1 or nxy3 > 4:
          raise Exception
      except:
        raise ValueError("Code[6] must be within 1-4")
      
      return True
    
    # conditions for 1000
    elif level == "1000":
      assert len(code) >= 8, "Code must be longer than 8 characters"
      try:
        xc = code[7].upper()
        if ord(xc) < ord("A") or ord(xc) > ord("E"):
          raise Exception
      except:
        raise ValueError("The upper case of Code[7] must be within A-E")
      try:
        ny4 = int(code[6])
        if ny4 < 0 or ny4 > 4:
          raise Exception
      except:
        raise ValueError("Code[6] must be within 0-4")
      
      return True
    
    # if the level is 500, calculate 500 level coordinates
    elif level == "500":
      assert len(code) >= 8, "Code must be longer than 8 characters"
      try:
        _ = int(code[7])
        _ = int(code[6])
      except:
        raise ValueError("Code[6] and Code[7] must be within 0-9")
        
      return True
      
  def _xyToCode(self, x:float, y:float, level:str = "50000") -> str:
    assert level in self.MESH_UNIT.keys(), "Level must be specified from: " + self.MESH_UNIT.keys()
    xy_idx = self.cellXyIdx(x, y)[0]
    
    scales = {
      k: int(v["scale"] / self.MESH_UNIT[level]["scale"]) 
      for k, v in self.MESH_UNIT.items()
      if k in self.MESH_UNIT[level]["keys_use"]
    }
    
    idxs = {}
    x_idx_r, y_idx_r = xy_idx
    for k, v in scales.items():
      idxs["x_" + k] = x_idx_r // v
      idxs["y_" + k] = y_idx_r // v
      x_idx_r = x_idx_r % v
      y_idx_r = y_idx_r % v
        
    if idxs["x_50000"] > 7 or idxs["y_50000"] > 19:
      raise ValueError("The coordinates are out of the tile boundary.")
    
    x_code = chr(ord("A") + idxs["x_50000"])
    y_code = chr(ord("A") + idxs["y_50000"])
    idxs["code_50000"] = f"{self.CRSCODE}{y_code}{x_code}"
    
    if level == "2500":
      idxs["xy_2500"] = 1 + idxs["x_2500"] + 2 * idxs["y_2500"]      
    elif level == "1000":
      idxs["x_1000"][0] = chr(ord("A") + idxs["x_1000"])
       
    return self.MESH_UNIT[level]["fmt"].format(**idxs)
    
  def _codeToXy(self, code:str, level:str = "50000") -> Tuple[float, float]:
    assert level in self.MESH_UNIT.keys(), "Level must be specified from: " + self.MESH_UNIT.keys()
    self._checkZukakuCode(code, level)
    code = code.upper()
    
    xc, yc = self.origin()
    if "50000" in self.MESH_UNIT[level]["keys_use"]:
      dx, dy = self.dxdy("50000")
      xc += dx * (ord(code[3]) - ord("A"))
      yc += dy * (ord(code[2]) - ord("A"))
    if "5000" in self.MESH_UNIT[level]["keys_use"]:
      dx, dy = self.dxdy("5000")
      xc += dx * int(code[5])
      yc += dy * int(code[4])
    if "2500" in self.MESH_UNIT[level]["keys_use"]:
      dx, dy = self.dxdy("2500")
      xc += dx * (int(code[6]) % 2 - 1)
      yc += dy * ((int(code[6]) - 1) // 2)
    if "1000" in self.MESH_UNIT[level]["keys_use"]:
      dx, dy = self.dxdy("1000")
      xc += dx * (ord(code[7]) - ord("A"))
      yc += dy * int(code[6])
    if "500" in self.MESH_UNIT[level]["keys_use"]:
      dx, dy = self.dxdy("500")
      xc += dx * int(code[7])
      yc += dy * int(code[6])
    
    
    dx, dy = self.dxdy(level)
            
    return (xc + 0.5 * dx, yc + 0.5 * dy)
  
  def __init__(self, crs:QgsCoordinateReferenceSystem, level:str):
    assert level in self.MESH_UNIT.keys(), "The mesh level is not valid. It must be one of the following: " + ", ".join(self.MESH_UNIT.keys())
    
    self.X_ORIG = self.ORIG["x"]
    self.Y_ORIG = self.ORIG["y"]
    
    if isinstance(crs, str):
      crs = QgsCoordinateReferenceSystem(crs)
    assert crs.authid()[-4:] in self.COORDS_JA_ALL.keys(), "The CRS must be in the Japanese coordinate system"
    
    self.LEVEL = level
    self.CRSCODE = self.COORDS_JA_ALL[crs.authid()[-4:]]
    dx, dy = self.dxdy(level)    
    
    def zukaku_coding(x:float, y:float) -> str:
      return self._xyToCode(x, y, level)
  
    def zukaku_rev_coding(code:str) -> Union[float, float]:
      return self._codeToXy(code, level)
    
    
    super().__init__(
      crs = crs, 
      xunit = dx, yunit = dy, 
      xorig = self.X_ORIG, yorig = self.Y_ORIG, 
      coding_func = zukaku_coding, rev_coding_func = zukaku_rev_coding
    )
  